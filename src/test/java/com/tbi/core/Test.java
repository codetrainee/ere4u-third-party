/**
 * 
 */
package com.tbi.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shameem
 *
 */

class Entity {
    final int id;
    final String data;

    public long getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public Entity(int id, String data) {
        super();
        this.id = id;
        this.data = data;
    }

   
}

public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        Collection<Entity> entities = Arrays.asList(new Entity(1, "one"), new Entity(11, "eleven"), new Entity(100, "one hundred"));
        // get a collection of all the ids.
        List<String> ids = entities.stream().map(p -> 
        String.valueOf(p.getId())).collect(Collectors.toList());
        System.out.println(ids.toString());
        
        
    }

}
