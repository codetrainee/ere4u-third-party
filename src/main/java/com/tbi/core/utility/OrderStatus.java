/**
 * 
 */
package com.tbi.core.utility;

/**
 * @author shameem
 *
 */
public enum OrderStatus {
    
    NEW,
    ACCEPTED,
    PREPARED,
    COMPLETE,
    DISPATCH,
    READYFORPICKUP,
    REJECT,
    CANCEL,
    PLACED,
}
