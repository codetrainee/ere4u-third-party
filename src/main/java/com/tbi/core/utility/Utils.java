/**
 * 
 */
package com.tbi.core.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author shameem
 *
 */
public class Utils {
   
    //Staging Key for test orders 
    public static final String AUTH_KEY = "apikey biz_adm_clients_evNUmHleEXKZ:0243bbc2f05654a0fe70dabda86e6ad5e99ad324";

    public static Date getDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        format.format(date);
        return date;
    }
    
    public static Date convertStringToDate(String lastDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(lastDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    
    public static Date convertUnixTimeToDate(Long time) {
        Date date = new Date(time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        format.format(date);
        return date;
    }
    
}
