package com.tbi.core.utility;

public class Provider {
    
   public static final String URBANPIPER = "urbanPiper"; 
   public static final String ZOMATO = "zomato";
   public static final String UBER = "uber";
   public static final String SWIGGY = "swiggy";
   public static final String FOODPANDA = "foodPanda";
   public static final String WAITERAPP = "waiterAPP";
   public static final String TBIWEBSITE = "tbiwebSite";
}
