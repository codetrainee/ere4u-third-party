package com.tbi.core.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Category;
@Repository
public interface CategoryRepository  extends JpaRepository<Category, Integer> {

	public List<Category> findByStoreId(Integer storeId);
	Category findCategoryByName(String name);
	List<Category> findCategoryByNameAndStoreId(String name, Integer storeId);
	public Set<Category> findById(Integer id);
}
 