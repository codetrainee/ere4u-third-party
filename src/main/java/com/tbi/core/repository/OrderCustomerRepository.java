/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Customer;


/**
 * @author shameem
 *
 */
@Repository
public interface OrderCustomerRepository extends JpaRepository<Customer, Integer>{


}
