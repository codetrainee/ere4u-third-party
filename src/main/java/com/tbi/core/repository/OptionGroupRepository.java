/**
 * 
 */
package com.tbi.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.OptionGroup;

/**
 * @author shameem
 *
 */
@Repository
public interface OptionGroupRepository extends JpaRepository<OptionGroup, Integer> {
	
	public List<OptionGroup> findByRefId(List<String> refId);

	public List<OptionGroup> findById(Integer id);

	

}
