package com.tbi.core.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.Params;
@Transactional
public interface ParamRepository extends JpaRepository<Params, Integer> {
	
	/*@Query(value="SELECT o.typeValue from OTPDetail o where o.otp =:otp ",nativeQuery = true) */
	/*OTPDetail findByTypeValue(Integer otp);*/
	
	@Query("SELECT p from Params p where p.type =:type AND p.clientId =:clientId")    
    List<Params> findByTypeAndClientId(@Param("type") String type,@Param("clientId")Integer clientId);
	
	/* @Query("SELECT e from Employee e where e.employeeName =:name AND e.employeeRole =:role")
	   List<Employee> findByNameAndRole(@Param("name") String name,@Param("role")String role);*/


}
