/**
 * 
 */
package com.tbi.core.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Item;
import com.tbi.core.models.User;

/** 
 * @author shameem
 *
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE item SET upload_status=:uploadStatus WHERE id=:id", nativeQuery = true)
	void setUploadStatusByuploadStatusAndid(@Param("uploadStatus") Integer uploadStatus, @Param("id") Integer id);

	List<Item> findItemByRefId(String refId);

	List<Item> findItemByRefIdAndAndStoreId(String refId, Integer storeId);

	Item save(Integer id);

	public List<Item> findById(List<Integer> id);

}
