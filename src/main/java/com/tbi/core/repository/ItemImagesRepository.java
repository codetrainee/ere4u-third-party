package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.ItemImages;

@Repository
public interface ItemImagesRepository extends JpaRepository<ItemImages, Integer> {

}
