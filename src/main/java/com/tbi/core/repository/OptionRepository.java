package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Option;

@Repository
public interface OptionRepository extends JpaRepository<Option, Integer> {

}
