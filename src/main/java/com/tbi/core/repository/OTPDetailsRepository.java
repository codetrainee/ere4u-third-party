package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tbi.core.models.OTPDetail;

public interface OTPDetailsRepository extends JpaRepository<OTPDetail, Integer> {
	
	public OTPDetail findByTypeValue(String typeValue);
	public OTPDetail findByOtp(Integer opt);
	public OTPDetail findTopByOrderByIdDesc();

}
