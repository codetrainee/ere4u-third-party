/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.ThirdParty;

/**
 * @author shameem
 *
 */
@Repository
public interface ThirdPartyRepository  extends JpaRepository<ThirdParty, Integer>{

	ThirdParty findById(Integer thirdPartyId);

}
