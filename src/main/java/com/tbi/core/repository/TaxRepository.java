/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Tax;

/**
 * @author shameem
 *
 */
@Repository
public interface TaxRepository  extends JpaRepository<Tax, Integer>{

}
