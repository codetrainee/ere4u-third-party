/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.DeliveryPerson;



/**
 * @author shameem
 *
 */
@Repository
public interface RiderRepository extends JpaRepository<DeliveryPerson, Integer> {

    

}
