package com.tbi.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.User;

/* Author Shrikant Mishra
	Date ==>7/07/2020
*/
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	public User findByMobileNo(Long mobileNo);
	public Optional<User> findByUserId(Long userId);
	public User findByUsername(String userName);


}
