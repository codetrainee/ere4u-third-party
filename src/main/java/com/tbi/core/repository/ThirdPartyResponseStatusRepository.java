/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.ThirdPartyResponseStatus;

/**
 * @author shameem
 *
 */
@Repository
public interface ThirdPartyResponseStatusRepository extends JpaRepository<ThirdPartyResponseStatus, Integer>{

}
