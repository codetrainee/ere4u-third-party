package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tbi.core.models.ClientRecharge;

public interface CustomerRechargeRepository extends JpaRepository<ClientRecharge, Integer>{

}
