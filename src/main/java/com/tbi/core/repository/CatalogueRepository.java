/**
 * 
 */
package com.tbi.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Catalogue;

/**
 * @author shameem
 *
 */
@Repository
public interface CatalogueRepository extends JpaRepository<Catalogue, Integer>{

    
}
