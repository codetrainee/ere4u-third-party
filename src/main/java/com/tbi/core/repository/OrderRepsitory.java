/**
 * 
 */
package com.tbi.core.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tbi.core.models.Order;
import com.tbi.core.models.User;

/**
 * @author shameem
 *
 */
@Repository
public interface OrderRepsitory extends JpaRepository<Order, Integer> {

    List<Order> findOrdersByStoreIdAndCurrentStateOrderByIdDesc(Integer storeId, String currentState);

    Order findOrderByRefId(Integer refId);


    List<Order> findByStoreIdAndCurrentState(Integer storeId, String currentState);
   
    List<Order> findByStoreIdAndUserId(Integer storeId,Long userId);
    List<Order> findOrdersByStoreIdAndCurrentState(@Param("store_id") Integer storeId, @Param("current_state") String currentState);

	Order findTopByOrderByRefIdDesc();

} 
