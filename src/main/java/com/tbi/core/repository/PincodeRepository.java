package com.tbi.core.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.tbi.core.models.Pincode;
import com.tbi.core.models.Store;

public interface PincodeRepository extends JpaRepository<Pincode, Integer>{
	 public Pincode findPincodeByCodeAndStore(String code,Store store); 
}
