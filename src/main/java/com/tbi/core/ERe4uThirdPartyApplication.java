package com.tbi.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;
import com.tbi.core.service.ItemService;


@SpringBootApplication
@EnableJpaAuditing
public class ERe4uThirdPartyApplication implements CommandLineRunner {
	  @Autowired
	  ItemService itemService;
	  
	  public static void main(String[] args) {
		SpringApplication.run(ERe4uThirdPartyApplication.class, args);
	}
	 @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }   
   @Override
	public void run(String... args) throws Exception {
		itemService.init();
	}

}
