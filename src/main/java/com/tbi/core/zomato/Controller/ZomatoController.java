/**
 * 
 */
package com.tbi.core.zomato.Controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shameem
 *
 */
@RestController
@RequestMapping("/zomato/endpoint")
public class ZomatoController {

    @GetMapping("/order/status/{orderId}")
    public void getStatus() {
        
    }

    @PostMapping("/order")
    public void order() {

    }

    @PutMapping("/order/status")
    public void status() {

    }

    @PutMapping("/order/rider/status")
    public void riderStatus() {

    }

    @DeleteMapping("/order/delete/{Id}")
    public void delete() {

    }
}
