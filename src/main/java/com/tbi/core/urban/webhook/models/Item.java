/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author shameem
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Item extends ResponseActions{
    
    private Boolean available;
    private List<String> category_ref_ids;
    private Integer current_stock;
    private String description;
    private List<String> excluded_platforms;
    private String food_type;
    private Integer price;
    private Boolean recommended;
    private String ref_id;
    private Boolean sold_at_store;
    private Tags tags;
    private String title;
    private List<Translations> translations;
    private UpiprStatus upipr_status;
    private List<Item> items;
    private String status ;

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public List<String> getCategory_ref_ids() {
        return category_ref_ids;
    }

    public void setCategory_ref_ids(List<String> category_ref_ids) {
        this.category_ref_ids = category_ref_ids;
    }

    public Integer getCurrent_stock() {
        return current_stock;
    }

    public void setCurrent_stock(Integer current_stock) {
        this.current_stock = current_stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getExcluded_platforms() {
        return excluded_platforms;
    }

    public void setExcluded_platforms(List<String> excluded_platforms) {
        this.excluded_platforms = excluded_platforms;
    }

    public String getFood_type() {
        return food_type;
    }

    public void setFood_type(String food_type) {
        this.food_type = food_type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public Boolean getSold_at_store() {
        return sold_at_store;
    }

    public void setSold_at_store(Boolean sold_at_store) {
        this.sold_at_store = sold_at_store;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Translations> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Translations> translations) {
        this.translations = translations;
    }

    public UpiprStatus getUpipr_status() {
        return upipr_status;
    }

    public void setUpipr_status(UpiprStatus upipr_status) {
        this.upipr_status = upipr_status;
    }

    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    

}
