/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Result {
	private String level;
	private List<Entities> entities;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public List<Entities> getEntities() {
		return entities;
	}

	public void setEntities(List<Entities> entities) {
		this.entities = entities;
	}

}
