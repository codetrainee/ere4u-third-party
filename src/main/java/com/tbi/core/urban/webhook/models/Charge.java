/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Charge {
    
    private Boolean active;
    private String description;
    private String ref_id;
    private String title;
    private Structure structure;
    private List<String> excluded_platforms;
    private List<String> fulfillment_modes;
    private List<String> item_ref_ids;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getExcluded_platforms() {
        return excluded_platforms;
    }

    public void setExcluded_platforms(List<String> excluded_platforms) {
        this.excluded_platforms = excluded_platforms;
    }

    public List<String> getFulfillment_modes() {
        return fulfillment_modes;
    }

    public void setFulfillment_modes(List<String> fulfillment_modes) {
        this.fulfillment_modes = fulfillment_modes;
    }

    public List<String> getItem_ref_ids() {
        return item_ref_ids;
    }

    public void setItem_ref_ids(List<String> item_ref_ids) {
        this.item_ref_ids = item_ref_ids;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
