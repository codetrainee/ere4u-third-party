/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class ItemActionCallback {

        private String action;
        private String platform;
        private List<ItemActionCallbackStatus> status;
        private Long ts_utc ;
        /**
         * @return the action
         */
        public String getAction() {
            return action;
        }
        /**
         * @return the platform
         */
        public String getPlatform() {
            return platform;
        }
        /**
         * @return the status
         */
        public List<ItemActionCallbackStatus> getStatus() {
            return status;
        }
      
        /**
         * @param action the action to set
         */
        public void setAction(String action) {
            this.action = action;
        }
        /**
         * @param platform the platform to set
         */
        public void setPlatform(String platform) {
            this.platform = platform;
        }
        /**
         * @param status the status to set
         */
        public void setStatus(List<ItemActionCallbackStatus> status) {
            this.status = status;
        }
        /**
         * @return the ts_utc
         */
        public Long getTs_utc() {
            return ts_utc;
        }
        /**
         * @param ts_utc the ts_utc to set
         */
        public void setTs_utc(Long ts_utc) {
            this.ts_utc = ts_utc;
        }
      
    
        
        
}
