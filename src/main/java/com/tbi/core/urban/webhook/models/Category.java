/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Category extends ResponseActions{

	private Boolean active;
	private String description;
	private String img_url;
	private String name;
	private String ref_id;
	private Integer sort_order;
	private List<Translations> translations;
	private UpiprStatus upipr_status;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRef_id() {
		return ref_id;
	}

	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public Integer getSort_order() {
		return sort_order;
	}

	public void setSort_order(Integer sort_order) {
		this.sort_order = sort_order;
	}

	public List<Translations> getTranslations() {
		return translations;
	}

	public void setTranslations(List<Translations> translations) {
		this.translations = translations;
	}

	public UpiprStatus getUpipr_status() {
		return upipr_status;
	}

	public void setUpipr_status(UpiprStatus upipr_status) {
		this.upipr_status = upipr_status;
	}

}
