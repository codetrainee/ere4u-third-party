/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Extras {
    
    private Float cash_to_be_collected;

    /**
     * @return the cash_to_be_collected
     */
    public Float getCash_to_be_collected() {
        return cash_to_be_collected;
    }

    /**
     * @param cash_to_be_collected the cash_to_be_collected to set
     */
    public void setCash_to_be_collected(Float cash_to_be_collected) {
        this.cash_to_be_collected = cash_to_be_collected;
    }
    
    

}
