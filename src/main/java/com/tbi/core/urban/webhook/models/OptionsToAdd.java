/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class OptionsToAdd {
    private Integer id;
    private String merchant_id;
    private Float price;
    private String title;
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the merchant_id
     */
    public String getMerchant_id() {
        return merchant_id;
    }
    /**
     * @param merchant_id the merchant_id to set
     */
    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }
    /**
     * @return the price
     */
    public Float getPrice() {
        return price;
    }
    /**
     * @param price the price to set
     */
    public void setPrice(Float price) {
        this.price = price;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    

}
