/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class RiderWebhook {

    private AdditionalInfo additional_info;
    private DeliveryInfo delivery_info;
    private Integer order_id;

    /**
     * @return the additional_info
     */
    public AdditionalInfo getAdditional_info() {
        return additional_info;
    }

    /**
     * @param additional_info
     *            the additional_info to set
     */
    public void setAdditional_info(AdditionalInfo additional_info) {
        this.additional_info = additional_info;
    }

    /**
     * @return the delivery_info
     */
    public DeliveryInfo getDelivery_info() {
        return delivery_info;
    }

    /**
     * @param delivery_info
     *            the delivery_info to set
     */
    public void setDelivery_info(DeliveryInfo delivery_info) {
        this.delivery_info = delivery_info;
    }

    /**
     * @return the order_id
     */
    public Integer getOrder_id() {
        return order_id;
    }

    /**
     * @param order_id the order_id to set
     */
    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

}
