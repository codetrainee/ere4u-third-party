/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Structure {

	private String applicable_on;
	private String type;
	private Float value;

	public String getApplicable_on() {
		return applicable_on;
	}

	public void setApplicable_on(String applicable_on) {
		this.applicable_on = applicable_on;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }

}
