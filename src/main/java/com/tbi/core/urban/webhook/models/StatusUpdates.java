/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class StatusUpdates {
    
    private String comments;
    private Long created;
    private String status;
    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }
    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }
    
    /**
     * @return the created
     */
    public Long getCreated() {
        return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Long created) {
        this.created = created;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    

}
