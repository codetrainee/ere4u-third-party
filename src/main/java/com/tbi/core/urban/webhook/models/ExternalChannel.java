/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class ExternalChannel {

	private String name;
	private String order_id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	
	
}
