/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class OrderStatusUpdateWebhookResponse {
	
		private AdditionalInfo additional_info;
 	    private String new_state;
	    private String order_id;
	    private String prev_state;
	    private String timestamp;
	    private String store_id;
	    private Long timestamp_unix;
	    private String message;
	    
		/**
         * @return the additional_info
         */
        public AdditionalInfo getAdditional_info() {
            return additional_info;
        }
        /**
         * @param additional_info the additional_info to set
         */
        public void setAdditional_info(AdditionalInfo additional_info) {
            this.additional_info = additional_info;
        }
        public String getNew_state() {
			return new_state;
		}
		public void setNew_state(String new_state) {
			this.new_state = new_state;
		}
		public String getOrder_id() {
			return order_id;
		}
		public void setOrder_id(String order_id) {
			this.order_id = order_id;
		}
		public String getPrev_state() {
			return prev_state;
		}
		public void setPrev_state(String prev_state) {
			this.prev_state = prev_state;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		public String getStore_id() {
			return store_id;
		}
		public void setStore_id(String store_id) {
			this.store_id = store_id;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
        /**
         * @return the timestamp_unix
         */
        public Long getTimestamp_unix() {
            return timestamp_unix;
        }
        /**
         * @param timestamp_unix the timestamp_unix to set
         */
        public void setTimestamp_unix(Long timestamp_unix) {
            this.timestamp_unix = timestamp_unix;
        }
		
		
}
