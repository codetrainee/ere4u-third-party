/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Stats {
    
    private Item items;
	private Category categories;
	private OptionGroups option_groups;
	private Option options;
	private Integer updated;
	private Integer errors;
	private Integer created;
	
	
	public Integer getUpdated() {
		return updated;
	}

	public void setUpdated(Integer updated) {
		this.updated = updated;
	}

	public Integer getErrors() {
		return errors;
	}

	public void setErrors(Integer errors) {
		this.errors = errors;
	}

	public Integer getCreated() {
		return created;
	}

	public void setCreated(Integer created) {
		this.created = created;
	}

    /**
     * @return the items
     */
    public Item getItems() {
        return items;
    }

    /**
     * @return the categories
     */
    public Category getCategories() {
        return categories;
    }

    /**
     * @return the option_groups
     */
    public OptionGroups getOption_groups() {
        return option_groups;
    }

    /**
     * @return the options
     */
    public Option getOptions() {
        return options;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Item items) {
        this.items = items;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(Category categories) {
        this.categories = categories;
    }

    /**
     * @param option_groups the option_groups to set
     */
    public void setOption_groups(OptionGroups option_groups) {
        this.option_groups = option_groups;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(Option options) {
        this.options = options;
    }

	
}
