/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

import com.tbi.core.urban.models.Store;

/**
 * @author shameem
 *
 */
public class StoreCreationWebhookResponse {

	private String reference;
	private Stats stats;
	private List<Store> stores;

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Stats getStats() {
		return stats;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}

	public List<Store> getStores() {
		return stores;
	}

	public void setStores(List<Store> stores) {
		this.stores = stores;
	}

}
