/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class DeliveryPersonDetails {
    
    private String alt_phone;
    private String name;
    private String phone;
    /**
     * @return the alt_phone
     */
    public String getAlt_phone() {
        return alt_phone;
    }
    /**
     * @param alt_phone the alt_phone to set
     */
    public void setAlt_phone(String alt_phone) {
        this.alt_phone = alt_phone;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }
    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    

}
