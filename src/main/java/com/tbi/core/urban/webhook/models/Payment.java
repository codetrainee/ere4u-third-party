/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Payment {
    private Float amount;
    private String option;
    private String srvr_trx_id;
    
    
   
    /**
     * @return the amount
     */
    public Float getAmount() {
        return amount;
    }
    /**
     * @param amount the amount to set
     */
    public void setAmount(Float amount) {
        this.amount = amount;
    }
    /**
     * @return the option
     */
    public String getOption() {
        return option;
    }
    /**
     * @param option the option to set
     */
    public void setOption(String option) {
        this.option = option;
    }
    /**
     * @return the srvr_trx_id
     */
    public String getSrvr_trx_id() {
        return srvr_trx_id;
    }
    /**
     * @param srvr_trx_id the srvr_trx_id to set
     */
    public void setSrvr_trx_id(String srvr_trx_id) {
        this.srvr_trx_id = srvr_trx_id;
    }
    
    
}
