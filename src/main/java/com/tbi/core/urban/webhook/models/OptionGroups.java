/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class OptionGroups extends ResponseActions{
    private Boolean active;
    private List<String> item_ref_ids;
    private Integer max_selectable;
    private Integer min_selectable;
    private String ref_id;
    private String title;
    private UpiprStatus upipr_status;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<String> getItem_ref_ids() {
        return item_ref_ids;
    }

    public void setItem_ref_ids(List<String> item_ref_ids) {
        this.item_ref_ids = item_ref_ids;
    }

    public Integer getMax_selectable() {
        return max_selectable;
    }

    public void setMax_selectable(Integer max_selectable) {
        this.max_selectable = max_selectable;
    }

    public Integer getMin_selectable() {
        return min_selectable;
    }

    public void setMin_selectable(Integer min_selectable) {
        this.min_selectable = min_selectable;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UpiprStatus getUpipr_status() {
        return upipr_status;
    }

    public void setUpipr_status(UpiprStatus upipr_status) {
        this.upipr_status = upipr_status;
    }

}
