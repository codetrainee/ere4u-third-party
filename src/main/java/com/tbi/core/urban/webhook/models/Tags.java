/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Tags {
    
    private List<String> swiggy;
    
    private List<String> zomato;

    public List<String> getSwiggy() {
        return swiggy;
    }

    public void setSwiggy(List<String> swiggy) {
        this.swiggy = swiggy;
    }

    public List<String> getZomato() {
        return zomato;
    }

    public void setZomato(List<String> zomato) {
        this.zomato = zomato;
    }
}
