/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class StoreActionWebhooksResponse {
	
	private String action;
	private String location_ref_id;
	private String platform;
	private boolean status;
	private List<Result> result;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getLocation_ref_id() {
		return location_ref_id;
	}
	public void setLocation_ref_id(String location_ref_id) {
		this.location_ref_id = location_ref_id;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public List<Result> getResult() {
		return result;
	}
	public void setResult(List<Result> result) {
		this.result = result;
	}
}
