/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class ExternalPlatforms {
    private String id;
    private String delivery_type;
    private String name;
    private String kind;
    private Extras extras;
    private List<Discount> discounts;
    
    
    
   
    /**
     * @return the discounts
     */
    public List<Discount> getDiscounts() {
        return discounts;
    }
    /**
     * @param discounts the discounts to set
     */
    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the delivery_type
     */
    public String getDelivery_type() {
        return delivery_type;
    }
    /**
     * @param delivery_type the delivery_type to set
     */
    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the kind
     */
    public String getKind() {
        return kind;
    }
    /**
     * @param kind the kind to set
     */
    public void setKind(String kind) {
        this.kind = kind;
    }
    /**
     * @return the extras
     */
    public Extras getExtras() {
        return extras;
    }
    /**
     * @param extras the extras to set
     */
    public void setExtras(Extras extras) {
        this.extras = extras;
    }
    
    
    
    
}

