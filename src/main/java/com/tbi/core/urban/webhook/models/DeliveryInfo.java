/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class DeliveryInfo {
    
    private String current_state;
    private DeliveryPersonDetails delivery_person_details;
    private List<StatusUpdates> status_updates;
    /**
     * @return the current_state
     */
    public String getCurrent_state() {
        return current_state;
    }
    /**
     * @param current_state the current_state to set
     */
    public void setCurrent_state(String current_state) {
        this.current_state = current_state;
    }
    /**
     * @return the delivery_person_details
     */
    public DeliveryPersonDetails getDelivery_person_details() {
        return delivery_person_details;
    }
    /**
     * @param delivery_person_details the delivery_person_details to set
     */
    public void setDelivery_person_details(DeliveryPersonDetails delivery_person_details) {
        this.delivery_person_details = delivery_person_details;
    }
    /**
     * @return the status_updates
     */
    public List<StatusUpdates> getStatus_updates() {
        return status_updates;
    }
    /**
     * @param status_updates the status_updates to set
     */
    public void setStatus_updates(List<StatusUpdates> status_updates) {
        this.status_updates = status_updates;
    }

    
}
