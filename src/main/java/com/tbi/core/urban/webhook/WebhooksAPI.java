/**
 * 
 */
package com.tbi.core.urban.webhook;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tbi.core.urban.models.Webhook;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author shameem
 *
 */
@RestController
@RequestMapping("webhooks")
public class WebhooksAPI {
	
	
	@PostMapping
	public Response save(@org.springframework.web.bind.annotation.RequestBody Webhook webhook) throws Exception {
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		ObjectMapper mapper = new ObjectMapper();
		RequestBody body = RequestBody.create(mediaType,mapper.writeValueAsString(webhook));
		Request request = new Request.Builder()
		  .url("https://staging.urbanpiper.com/external/api/v1/webhooks/")
		  .post(body)
		  .addHeader("Authorization", "apikey testusername:testapikeys")
		  .addHeader("Content-Type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();
		return client.newCall(request).execute();
	}
	
	@GetMapping
	public Response getAll() throws Exception {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
		  .url("https://staging.urbanpiper.com/external/api/v1/webhooks/")
		  .get()
		  .addHeader("Authorization", "apikey testusername:testapikeys")
		  .addHeader("Content-Type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();
		return client.newCall(request).execute();
	}
	
	@GetMapping("/{id}")
	public Response getById(@PathVariable(value = "id")  String id) throws Exception {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
		  .url("https://staging.urbanpiper.com/external/api/v1/webhooks/"+id)
		  .get()
		  .addHeader("Authorization", "apikey testusername:testapikeys")
		  .addHeader("Content-Type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();
		return client.newCall(request).execute();
	}
	
	@PutMapping("/{id}")
	public Response updateById(@PathVariable(value = "id")  String id) throws Exception {
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,id);
		Request request = new Request.Builder()
		  .url("https://staging.urbanpiper.com/external/api/v1/webhooks/")
		  .post(body)
		  .addHeader("Authorization", "apikey testusername:testapikeys")
		  .addHeader("Content-Type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();
		return client.newCall(request).execute();
	}

}
