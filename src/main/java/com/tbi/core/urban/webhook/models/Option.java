/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Option extends ResponseActions{

    private Boolean available;
    private String description;
    private List<String> nested_opt_grps;
    private List<String> opt_grp_ref_ids;
    private Integer price;
    private String ref_id;
    private Boolean sold_at_store;
    private String title;
    private List<Translations> translations;
    private UpiprStatus upipr_status;
    private Integer weight;
    
    
    public Boolean getAvailable() {
        return available;
    }
    public void setAvailable(Boolean available) {
        this.available = available;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public List<String> getNested_opt_grps() {
        return nested_opt_grps;
    }
    public void setNested_opt_grps(List<String> nested_opt_grps) {
        this.nested_opt_grps = nested_opt_grps;
    }
    public List<String> getOpt_grp_ref_ids() {
        return opt_grp_ref_ids;
    }
    public void setOpt_grp_ref_ids(List<String> opt_grp_ref_ids) {
        this.opt_grp_ref_ids = opt_grp_ref_ids;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }
    public String getRef_id() {
        return ref_id;
    }
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }
    public Boolean getSold_at_store() {
        return sold_at_store;
    }
    public void setSold_at_store(Boolean sold_at_store) {
        this.sold_at_store = sold_at_store;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public List<Translations> getTranslations() {
        return translations;
    }
    public void setTranslations(List<Translations> translations) {
        this.translations = translations;
    }
    public UpiprStatus getUpipr_status() {
        return upipr_status;
    }
    public void setUpipr_status(UpiprStatus upipr_status) {
        this.upipr_status = upipr_status;
    }
    public Integer getWeight() {
        return weight;
    }
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
