/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class ResponseActions {
    
    private Integer created;
    private Integer deleted; 
    private Integer errors;
    private Integer updated;
    /**
     * @return the created
     */
    public Integer getCreated() {
        return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Integer created) {
        this.created = created;
    }
    /**
     * @return the deleted
     */
    public Integer getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the errors
     */
    public Integer getErrors() {
        return errors;
    }
    /**
     * @param errors the errors to set
     */
    public void setErrors(Integer errors) {
        this.errors = errors;
    }
    /**
     * @return the updated
     */
    public Integer getUpdated() {
        return updated;
    }
    /**
     * @param updated the updated to set
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }
    
    
    

}
