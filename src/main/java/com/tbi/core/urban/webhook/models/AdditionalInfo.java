/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class AdditionalInfo {

	private ExternalChannel external_channel;

    /**
     * @return the external_channel
     */
    public ExternalChannel getExternal_channel() {
        return external_channel;
    }

    /**
     * @param external_channel the external_channel to set
     */
    public void setExternal_channel(ExternalChannel external_channel) {
        this.external_channel = external_channel;
    }
}
