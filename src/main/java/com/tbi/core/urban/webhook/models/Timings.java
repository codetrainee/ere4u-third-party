/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class Timings {

	private String day;
	private List<Slots> slots;

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public List<Slots> getSlots() {
		return slots;
	}

	public void setSlots(List<Slots> slots) {
		this.slots = slots;
	}

}
