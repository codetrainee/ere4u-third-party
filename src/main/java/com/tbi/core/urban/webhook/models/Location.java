/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Location {
    private Integer id;
    private String ref_id;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

   

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }



    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }



    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    

}
