/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;

import com.tbi.core.urban.models.Store;

/**
 * @author shameem
 *
 */
public class Order {
    
    private Details details;
    private List<OrderItems> items;
    private String next_state;
    private List<String> next_states;
    private List<Payment> payment;
    private Store store;
    
    /**
     * @return the details
     */
    public Details getDetails() {
        return details;
    }
    /**
     * @param details the details to set
     */
    public void setDetails(Details details) {
        this.details = details;
    }
    /**
     * @return the items
     */
    public List<OrderItems> getItems() {
        return items;
    }
    /**
     * @param items the items to set
     */
    public void setItems(List<OrderItems> items) {
        this.items = items;
    }
    /**
     * @return the next_state
     */
    public String getNext_state() {
        return next_state;
    }
    /**
     * @param next_state the next_state to set
     */
    public void setNext_state(String next_state) {
        this.next_state = next_state;
    }
    /**
     * @return the next_states
     */
    public List<String> getNext_states() {
        return next_states;
    }
    /**
     * @param next_states the next_states to set
     */
    public void setNext_states(List<String> next_states) {
        this.next_states = next_states;
    }
    /**
     * @return the payment
     */
    public List<Payment> getPayment() {
        return payment;
    }
    /**
     * @param payment the payment to set
     */
    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }
    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }
    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }
    
    

}
