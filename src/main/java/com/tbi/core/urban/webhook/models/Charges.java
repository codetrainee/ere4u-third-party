package com.tbi.core.urban.webhook.models;

public class Charges {
    
    private String title;
    private Float value;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }
   

    
}
