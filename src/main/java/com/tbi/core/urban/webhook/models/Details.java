/**
 * 
 */
package com.tbi.core.urban.webhook.models;

import java.util.List;



/**
 * @author shameem
 *
 */
public class Details {

    private String channel;
    private List<Charges> charges;
    private String coupon;
    
    private Long created;
    private Long delivery_datetime;
    
    private Float discount;
    private List<ExternalPlatforms> ext_platforms;
    private Integer id;
    private String instructions;
    private Float item_level_total_charges;
    private Float item_level_total_taxes;
    private Float item_taxes;
    private String merchant_ref_id;
    private Float order_level_total_charges;
    private Float order_level_total_taxes;
    private String order_state;
    private Float order_subtotal;
    private Float order_total;
    private String order_type;
    private String state;
    private List<Tax> taxes;
    private Float total_charges;
    private Float total_external_discount;
    private Float total_taxes;
    
    /**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }
    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }
    /**
     * @return the charges
     */
    public List<Charges> getCharges() {
        return charges;
    }
    /**
     * @param charges the charges to set
     */
    public void setCharges(List<Charges> charges) {
        this.charges = charges;
    }
    /**
     * @return the coupon
     */
    public String getCoupon() {
        return coupon;
    }
    /**
     * @param coupon the coupon to set
     */
    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
    
    /**
     * @return the created
     */
    public Long getCreated() {
        return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Long created) {
        this.created = created;
    }
  
    /**
     * @return the discount
     */
    public Float getDiscount() {
        return discount;
    }
    /**
     * @param discount the discount to set
     */
    public void setDiscount(Float discount) {
        this.discount = discount;
    }
    /**
     * @return the ext_platforms
     */
    public List<ExternalPlatforms> getExt_platforms() {
        return ext_platforms;
    }
    /**
     * @param ext_platforms the ext_platforms to set
     */
    public void setExt_platforms(List<ExternalPlatforms> ext_platforms) {
        this.ext_platforms = ext_platforms;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the instructions
     */
    public String getInstructions() {
        return instructions;
    }
    /**
     * @param instructions the instructions to set
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
    /**
     * @return the item_level_total_charges
     */
    public Float getItem_level_total_charges() {
        return item_level_total_charges;
    }
    /**
     * @param item_level_total_charges the item_level_total_charges to set
     */
    public void setItem_level_total_charges(Float item_level_total_charges) {
        this.item_level_total_charges = item_level_total_charges;
    }
    /**
     * @return the item_level_total_taxes
     */
    public Float getItem_level_total_taxes() {
        return item_level_total_taxes;
    }
    /**
     * @param item_level_total_taxes the item_level_total_taxes to set
     */
    public void setItem_level_total_taxes(Float item_level_total_taxes) {
        this.item_level_total_taxes = item_level_total_taxes;
    }
    /**
     * @return the item_taxes
     */
    public Float getItem_taxes() {
        return item_taxes;
    }
    /**
     * @param item_taxes the item_taxes to set
     */
    public void setItem_taxes(Float item_taxes) {
        this.item_taxes = item_taxes;
    }
    /**
     * @return the merchant_ref_id
     */
    public String getMerchant_ref_id() {
        return merchant_ref_id;
    }
    /**
     * @param merchant_ref_id the merchant_ref_id to set
     */
    public void setMerchant_ref_id(String merchant_ref_id) {
        this.merchant_ref_id = merchant_ref_id;
    }
    /**
     * @return the order_level_total_charges
     */
    public Float getOrder_level_total_charges() {
        return order_level_total_charges;
    }
    /**
     * @param order_level_total_charges the order_level_total_charges to set
     */
    public void setOrder_level_total_charges(Float order_level_total_charges) {
        this.order_level_total_charges = order_level_total_charges;
    }
    /**
     * @return the order_level_total_taxes
     */
    public Float getOrder_level_total_taxes() {
        return order_level_total_taxes;
    }
    /**
     * @param order_level_total_taxes the order_level_total_taxes to set
     */
    public void setOrder_level_total_taxes(Float order_level_total_taxes) {
        this.order_level_total_taxes = order_level_total_taxes;
    }
    /**
     * @return the order_state
     */
    public String getOrder_state() {
        return order_state;
    }
    /**
     * @param order_state the order_state to set
     */
    public void setOrder_state(String order_state) {
        this.order_state = order_state;
    }
    /**
     * @return the order_subtotal
     */
    public Float getOrder_subtotal() {
        return order_subtotal;
    }
    /**
     * @param order_subtotal the order_subtotal to set
     */
    public void setOrder_subtotal(Float order_subtotal) {
        this.order_subtotal = order_subtotal;
    }
    /**
     * @return the order_total
     */
    public Float getOrder_total() {
        return order_total;
    }
    /**
     * @param order_total the order_total to set
     */
    public void setOrder_total(Float order_total) {
        this.order_total = order_total;
    }
    
    /**
     * @return the order_type
     */
    public String getOrder_type() {
        return order_type;
    }
    /**
     * @param order_type the order_type to set
     */
    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }
    /**
     * @return the state
     */
    public String getState() {
        return state;
    }
    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }
    /**
     * @return the taxes
     */
    public List<Tax> getTaxes() {
        return taxes;
    }
    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }
    /**
     * @return the total_charges
     */
    public Float getTotal_charges() {
        return total_charges;
    }
    /**
     * @param total_charges the total_charges to set
     */
    public void setTotal_charges(Float total_charges) {
        this.total_charges = total_charges;
    }
    /**
     * @return the total_external_discount
     */
    public Float getTotal_external_discount() {
        return total_external_discount;
    }
    /**
     * @param total_external_discount the total_external_discount to set
     */
    public void setTotal_external_discount(Float total_external_discount) {
        this.total_external_discount = total_external_discount;
    }
    /**
     * @return the total_taxes
     */
    public Float getTotal_taxes() {
        return total_taxes;
    }
    /**
     * @param total_taxes the total_taxes to set
     */
    public void setTotal_taxes(Float total_taxes) {
        this.total_taxes = total_taxes;
    }
    /**
     * @return the delivery_datetime
     */
    public Long getDelivery_datetime() {
        return delivery_datetime;
    }
    /**
     * @param delivery_datetime the delivery_datetime to set
     */
    public void setDelivery_datetime(Long delivery_datetime) {
        this.delivery_datetime = delivery_datetime;
    }
    

}
