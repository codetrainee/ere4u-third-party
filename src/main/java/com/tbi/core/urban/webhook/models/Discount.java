/**
 * 
 */
package com.tbi.core.urban.webhook.models;

/**
 * @author shameem
 *
 */
public class Discount {
    
    private Boolean is_merchant_discount;
    private Float rate;
    private String title;
    private Float value;
    private String code;
    /**
     * @return the is_merchant_discount
     */
    public Boolean getIs_merchant_discount() {
        return is_merchant_discount;
    }
    /**
     * @return the rate
     */
    public Float getRate() {
        return rate;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    /**
     * @param is_merchant_discount the is_merchant_discount to set
     */
    public void setIs_merchant_discount(Boolean is_merchant_discount) {
        this.is_merchant_discount = is_merchant_discount;
    }
    /**
     * @param rate the rate to set
     */
    public void setRate(Float rate) {
        this.rate = rate;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }
    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    
}
