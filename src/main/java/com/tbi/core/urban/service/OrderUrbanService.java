package com.tbi.core.urban.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tbi.core.models.ItemActionStatusUpdate;
import com.tbi.core.models.Order;
import com.tbi.core.models.Store;
import com.tbi.core.models.ThirdPartyResponseStatus;
import com.tbi.core.repository.ItemActionStatusRepository;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.repository.ThirdPartyResponseStatusRepository;
import com.tbi.core.service.CatalogueService;
import com.tbi.core.service.OrderService;
import com.tbi.core.service.ProviderService;
import com.tbi.core.urban.converter.UrbanPiperConverter;
import com.tbi.core.urban.models.Catalogue;
import com.tbi.core.urban.models.ItemAction;
import com.tbi.core.urban.models.OrderStatus;
import com.tbi.core.urban.models.StoreAction;
import com.tbi.core.urban.models.StoreUrban;
import com.tbi.core.urban.webhook.models.CatalogueWebhook;
import com.tbi.core.urban.webhook.models.ItemActionCallback;
import com.tbi.core.urban.webhook.models.OrderStatusUpdateWebhookResponse;
import com.tbi.core.urban.webhook.models.OrderWebhook;
import com.tbi.core.urban.webhook.models.RiderWebhook;
import com.tbi.core.urban.webhook.models.StoreActionWebhooksResponse;
import com.tbi.core.urban.webhook.models.StoreCreationWebhookResponse;
import com.tbi.core.utility.Provider;
import com.tbi.core.utility.Utils;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Service(Provider.URBANPIPER)
public class OrderUrbanService implements ProviderService {

    @Autowired
    OrderService orderService;

    @Autowired
    StoreRepository storeRepository;
 
    @Autowired
    CatalogueService catalogueService;

    @Autowired
    ItemActionStatusRepository itemActionStatusRepository;
    
    @Autowired
    ThirdPartyResponseStatusRepository thirdPartyResponseStatusRepository;

    @Autowired
    private Environment env;
    
   

    /* Order Api's Call Urban */
    @Override
    public Response updateOrderStatus(Order orderStatus, String authKey) throws Exception {
        OrderStatus orderStatusUrban = new OrderStatus();
        orderStatusUrban = UrbanPiperConverter.convertOrderToUrbanOrder(orderStatus, orderStatusUrban);
        return updateUrbanOrderStatus(orderStatusUrban, orderStatus.getRefId(), authKey);
    }
   
    private Response updateUrbanOrderStatus(OrderStatus orderStatus, Integer orderId, String authKey) throws Exception {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(orderStatus));
        RequestBody body = RequestBody.create(mediaType, mapper.writeValueAsString(orderStatus));
        Request request = new Request.Builder().url("https://staging.urbanpiper.com/external/api/v1/orders/"+ orderId + "/status/").put(body)
                .addHeader("Content-Type", "application/json").addHeader("Authorization", authKey).addHeader("Cache-Control", "no-cache").build();
        Response response = client.newCall(request).execute();
        System.out.println(response);
        return response; 
    }

    public void createNewOrder(OrderWebhook orderWebhook) {
        orderService.saveOrder(UrbanPiperConverter.convertUrbanOrderToOrder(orderWebhook));
    }

    public void statusUpdateOfUrbanOrder(OrderStatusUpdateWebhookResponse orderStatusWebhook) {
        Order order = orderService.findOrderByRefId(Integer.valueOf(orderStatusWebhook.getOrder_id()));
        orderService.saveOrder(UrbanPiperConverter.convertUrbanStatusToOrderStatus(orderStatusWebhook, order));
    }

    /* Update Rider Status */
    public void statusUpdateOfUrbanRider(RiderWebhook riderWebhook) {
        Order order = orderService.findOrderByRefId(riderWebhook.getOrder_id());
        order.setRiderDetails(UrbanPiperConverter.convertUrbanRiderToOrderRider(riderWebhook));
        orderService.saveOrder(order);
    }

    @Override
    public void updateRiderStatus(Order order, String authKey) {
        /*
         * TODO : update RiderStatus from TBI
         */
    }

    /* Store Api's Call Urban */
    @Override
    public Response saveStore(List<Store> storeList, String authKey) throws Exception {
        StoreUrban storeUrban = new StoreUrban();
        storeUrban = UrbanPiperConverter.convertStoreToStoreUrban(storeList, storeUrban);
        return callUrbanStoreApi(storeUrban, authKey);
    }

    public Response callUrbanStoreApi(StoreUrban storeUrban, String authKey) throws Exception {
        Response response = null;
        OkHttpClient client = new OkHttpClient();
        ObjectMapper mapper = new ObjectMapper();
        MediaType mediaType = MediaType.parse("application/json");
        System.out.println(mapper.writeValueAsString(storeUrban));
        RequestBody body = RequestBody.create(mediaType, mapper.writeValueAsString(storeUrban));
        Request request = new Request.Builder().url("https://staging.urbanpiper.com/external/api/v1/stores/").post(body).addHeader("Authorization", authKey)
                .addHeader("Content-Type", "application/json").build();
        response = client.newCall(request).execute();
        return response;
    }

    @Override
    public Response updateStoreAction(Store store, String authKey) throws Exception {
        StoreAction storeUrban = new StoreAction();
        storeUrban = UrbanPiperConverter.convertStoreActionToUrbanStoreAction(store, storeUrban);
        return updateStoreAction(storeUrban, authKey);
    }

    public Response updateStoreAction(StoreAction storeUrban, String authKey) throws Exception {
        OkHttpClient client = new OkHttpClient();
        ObjectMapper mapper = new ObjectMapper();
        MediaType mediaType = MediaType.parse("application/json");
        System.out.println(mapper.writeValueAsString(storeUrban));
        RequestBody body = RequestBody.create(mediaType, mapper.writeValueAsString(storeUrban));
        Request request = new Request.Builder().url("https://staging.urbanpiper.com/hub/api/v1/location/").post(body).addHeader("Content-Type", "application/json")
                .addHeader("Authorization", Utils.AUTH_KEY).build();
        return client.newCall(request).execute();
    }

    public void updateUrbanStoreConfigWebhookResponse(StoreCreationWebhookResponse store) {
        Store storeUpdate = storeRepository.findStoreByRefId(store.getStores().get(0).getRef_id());
        storeUpdate.setThirdPartyResponseStatus(UrbanPiperConverter.convertStoreCreationResponseToTbi(store, "Store creation"));
        storeRepository.save(storeUpdate);
    }

    public void updateStoreActionWebhookRes(StoreActionWebhooksResponse storeActionCallback) {
        Store store = new Store();
        List<String> platform = new ArrayList<>();
        store.setId(Integer.valueOf(storeActionCallback.getLocation_ref_id()));
        store.setAction(storeActionCallback.getAction());
        store.setActive(storeActionCallback.isStatus());
        platform.add(storeActionCallback.getPlatform());
        store.setPlatforms(platform);
    }

    @Override
    public Response saveCatalogue(Store store, String authKey) throws Exception {

           Catalogue urbanCatalogue = new Catalogue();
           com.tbi.core.models.Catalogue catalogue = store.getCatalogue();
           urbanCatalogue = UrbanPiperConverter.convertCatalogueToUrbanCatalogue(catalogue);
           return SaveCatalogue(urbanCatalogue, store.getRefId(), authKey);

    }

    private Response SaveCatalogue(com.tbi.core.urban.models.Catalogue urbanCatalogue, String storeId, String authKey) throws Exception {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(urbanCatalogue));
        RequestBody body = RequestBody.create(mediaType, mapper.writeValueAsString(urbanCatalogue));

    
        Request request = new Request.Builder().url("https://staging.urbanpiper.com/external/api/v1/inventory/locations/" + storeId + "/").post(body)

                .addHeader("Content-Type", "application/json").addHeader("Authorization", authKey).build();
        Response response = client.newCall(request).execute();
        return response;
    }

    @Override
    public Response updateItemAction(com.tbi.core.models.Catalogue catalogue, String storeId, String authKey) throws Exception {
        ItemAction itemAction = new ItemAction();
        UrbanPiperConverter.convertItemActionToUrbanItemAction(itemAction, catalogue, storeId);
        return updateItemAction(itemAction, authKey);
    }

    private Response updateItemAction(ItemAction itemAction, String authKey) throws Exception {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(itemAction));
        RequestBody body = RequestBody.create(mediaType, mapper.writeValueAsString(itemAction));

    
        Request request = new Request.Builder().url("https://staging.urbanpiper.com/hub/api/v1/items/").post(body).addHeader("Content-Type", "application/json")
                .addHeader("Authorization", authKey).addHeader("Cache-Control", "no-cache").build();
        Response response = client.newCall(request).execute();
        return response;
    }

    public ItemActionStatusUpdate itemActionCallbackUpdate(ItemActionCallback itemActionCallback) {
        if (itemActionCallback != null) {
            ItemActionStatusUpdate itemAction = UrbanPiperConverter.setUPItemActionToItem(itemActionCallback);
            return itemActionStatusRepository.save(itemAction);
        }
        return null;
    }

    public CatalogueWebhook updateCatalogueWebhookResponse(CatalogueWebhook catalogueWebhook) {
        List<ThirdPartyResponseStatus> partyResponseStatus = UrbanPiperConverter.convertWebhookResponseToEre4uResponse(catalogueWebhook);
        for (ThirdPartyResponseStatus thirdPartyResponseStatus : partyResponseStatus) {
            thirdPartyResponseStatusRepository.save(thirdPartyResponseStatus);
        }
        return null;
    }

}
