package com.tbi.core.urban.converter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.tbi.core.models.Catalogue;
import com.tbi.core.models.Charges;
import com.tbi.core.models.Customer;
import com.tbi.core.models.DeliveryPerson;
import com.tbi.core.models.DeliveryStatus;
import com.tbi.core.models.ExternalChannalInfo;
import com.tbi.core.models.ExternalDiscount;
import com.tbi.core.models.ItemActionStatusUpdate;
import com.tbi.core.models.ItemActionSummary;
import com.tbi.core.models.OptionGroup;
import com.tbi.core.models.OptionsToAdd;
import com.tbi.core.models.Order;
import com.tbi.core.models.OrderDetails;
import com.tbi.core.models.OrderItemTax;
import com.tbi.core.models.OrderItems;
import com.tbi.core.models.OrderPayments;
import com.tbi.core.models.OrderState;
import com.tbi.core.models.RiderDetails;
import com.tbi.core.models.Store;
import com.tbi.core.models.Tax;
import com.tbi.core.models.ThirdPartyResponseStatus;
import com.tbi.core.urban.models.Category;
import com.tbi.core.urban.models.Item;
import com.tbi.core.urban.models.ItemAction;
import com.tbi.core.urban.models.Option;
import com.tbi.core.urban.models.OptionGroups;
import com.tbi.core.urban.models.OrderStatus;
import com.tbi.core.urban.models.StoreAction;
import com.tbi.core.urban.models.StoreUrban;
import com.tbi.core.urban.webhook.models.CatalogueWebhook;
import com.tbi.core.urban.webhook.models.Charge;
import com.tbi.core.urban.webhook.models.DeliveryInfo;
import com.tbi.core.urban.webhook.models.Details;
import com.tbi.core.urban.webhook.models.ExternalChannel;
import com.tbi.core.urban.webhook.models.ExternalPlatforms;
import com.tbi.core.urban.webhook.models.ItemActionCallback;
import com.tbi.core.urban.webhook.models.ItemActionCallbackStatus;
import com.tbi.core.urban.webhook.models.ItemTax;
import com.tbi.core.urban.webhook.models.Location;
import com.tbi.core.urban.webhook.models.OrderStatusUpdateWebhookResponse;
import com.tbi.core.urban.webhook.models.OrderWebhook;
import com.tbi.core.urban.webhook.models.RiderWebhook;
import com.tbi.core.urban.webhook.models.Stats;
import com.tbi.core.urban.webhook.models.StoreCreationWebhookResponse;
import com.tbi.core.urban.webhook.models.Structure;
import com.tbi.core.urban.webhook.models.Tags;
import com.tbi.core.urban.webhook.models.Timings;
import com.tbi.core.utility.Provider;
import com.tbi.core.utility.Utils;

public final class UrbanPiperConverter {

    public static Order convertUrbanOrderToOrder(OrderWebhook orderWebhook) {
        List<OrderState> orderStates = new ArrayList<>();
        OrderState currentState = new OrderState();
        ModelMapper mapper = new ModelMapper();
        Order order = new Order();
        order.setProvider(Provider.URBANPIPER);
        OrderDetails details = new OrderDetails();
        com.tbi.core.urban.webhook.models.Order urbanOrder = orderWebhook.getOrder();
        order.setCurrentDate(Utils.getDate());
        order.setDetails(addOrderDetails(order, details, urbanOrder.getDetails(), mapper));
        currentState.setStatus(order.getCurrentState());
        currentState.setDate(Utils.getDate());
        orderStates.add(currentState);
        order.setOrderStates(orderStates);
        order.setCustomerDetails(mapper.map(orderWebhook.getCustomer(), Customer.class));
        order.setItems(addOrderItems(mapper, orderWebhook.getOrder()));
        Type listT = new TypeToken<List<OrderPayments>>() {
        }.getType();
        order.setPayment(mapper.map(urbanOrder.getPayment(), listT));
        order.setStoreId(Integer.valueOf(urbanOrder.getStore().getMerchant_ref_id()));
        return order;
    }

    private static List<OrderItems> addOrderItems(ModelMapper mapper, com.tbi.core.urban.webhook.models.Order order) {
        List<OrderItems> orderItemList = new ArrayList<OrderItems>();
        List<com.tbi.core.urban.webhook.models.OrderItems> Items = order.getItems();
        for (com.tbi.core.urban.webhook.models.OrderItems urbanItem : Items) {
            OrderItems orderItem = new OrderItems();
            Type urbanCharges = new TypeToken<List<Charges>>() {
            }.getType();
            orderItem.setCharges(mapper.map(urbanItem.getCharges(), urbanCharges));
            orderItem.setDiscount(urbanItem.getDiscount());
            orderItem.setFoodType(urbanItem.getFood_type());
            orderItem.setImage_landscape_url(urbanItem.getImage_landscape_url());
            orderItem.setImage_url(urbanItem.getImage_url());
            orderItem.setPrice(urbanItem.getPrice());
            orderItem.setQuantity(urbanItem.getQuantity());
            orderItem.setName(urbanItem.getTitle());
            orderItem.setTotal(urbanItem.getTotal());
            orderItem.setTotal_with_tax(urbanItem.getTotal_with_tax());
            orderItem.setUnit_weight(urbanItem.getUnit_weight());
            orderItem.setRefId(urbanItem.getMerchant_id().replace("I-", ""));
            List<OrderItemTax> itemTaxs = new ArrayList<>();
            for (ItemTax itemTax : urbanItem.getTaxes()) {
                OrderItemTax tax = new OrderItemTax();
                tax.setName(itemTax.getTitle().replace("(Created by UrbanPiper)", ""));
                tax.setRate(itemTax.getRate());
                tax.setValue(itemTax.getValue());
                itemTaxs.add(tax);
            }
            orderItem.setTaxes(itemTaxs);
            Type optionAdd = new TypeToken<List<OptionsToAdd>>() {
            }.getType();
            orderItem.setOptions_to_add(mapper.map(urbanItem.getOptions_to_add(), optionAdd));
            orderItemList.add(orderItem);
        }
        return orderItemList;
    }

    private static OrderDetails addOrderDetails(Order order, OrderDetails details, Details details2, ModelMapper mapper) {
        order.setChannel(details2.getChannel());
        order.setCurrentState(details2.getState());
        order.setRefId(details2.getId());
        details.setCoupon(details2.getCoupon());
        details.setCreated(Utils.convertUnixTimeToDate(details2.getCreated()));
        details.setDeliveryDateTime(Utils.convertUnixTimeToDate(details2.getDelivery_datetime()));
        details.setDiscount(details2.getDiscount());
        details.setInstructions(details2.getInstructions());
        details.setMerchant_ref_id(details2.getMerchant_ref_id());
        details.setItemTotalCharges(details2.getItem_level_total_charges());
        details.setItemTotalTaxes(details2.getItem_level_total_taxes());
        details.setItemTaxes(details2.getItem_taxes());
        details.setTotalCharges(details2.getOrder_level_total_charges());
        details.setTotalTaxes(details2.getOrder_level_total_taxes());
        details.setState(details2.getOrder_state());
        details.setSubtotal(details2.getOrder_subtotal());
        details.setTotal(details2.getOrder_total());
        details.setType(details2.getOrder_type());
        details.setState(details2.getState());
        details.setTotalCharges(details2.getTotal_charges());
        details.setTotalExternalDiscount(details2.getTotal_external_discount());
        details.setTotalTaxes(details2.getTotal_taxes());
        Type listType = new TypeToken<List<Tax>>() {
        }.getType();
        details.setTaxes(mapper.map(details2.getTaxes(), listType));
        Type urbanCharges = new TypeToken<List<Charges>>() {
        }.getType();
        details.setCharges(mapper.map(details2.getCharges(), urbanCharges));
        List<ExternalPlatforms> externalPlatforms = details2.getExt_platforms();
        ExternalChannalInfo channalInfo = new ExternalChannalInfo();
        List<ExternalDiscount> externalDiscounts = new ArrayList<ExternalDiscount>();
        for (ExternalPlatforms platforms : externalPlatforms) {
            channalInfo.setDeliveryType(platforms.getDelivery_type());
            channalInfo.setKind(platforms.getKind());
            channalInfo.setName(platforms.getName());
            if (platforms.getDiscounts() != null) {
                platforms.getDiscounts().forEach(dis -> {
                    ExternalDiscount discount = new ExternalDiscount();
                    discount.setName(dis.getTitle());
                    discount.setRate(dis.getRate());
                    discount.setValue(dis.getValue());
                    discount.setCode(dis.getCode());
                    externalDiscounts.add(discount);
                });
            }
            channalInfo.setExternalDiscountInfo(externalDiscounts);
        }
        order.setExtChannel(channalInfo);
        return details;
    }

    public static OrderStatus convertOrderToUrbanOrder(Order order, OrderStatus orderStatusUrban) {
        orderStatusUrban.setMessage(order.getMessage());
        orderStatusUrban.setNew_status(order.getCurrentState());
        return orderStatusUrban;
    }

    public static Order convertUrbanStatusToOrderStatus(OrderStatusUpdateWebhookResponse orderStatusWebhook, Order order) {
        List<OrderState> orderStates = new ArrayList<>();
        OrderState state = new OrderState();
        ExternalChannalInfo channalInfo = new ExternalChannalInfo();
        ExternalChannel info = orderStatusWebhook.getAdditional_info().getExternal_channel();
        channalInfo.setChannalOrderId(info.getOrder_id());
        channalInfo.setName(info.getName());
        order.setExtChannel(channalInfo);
        order.setCurrentState(orderStatusWebhook.getNew_state());
        order.setPreviousState(orderStatusWebhook.getPrev_state());
        order.setMessage(orderStatusWebhook.getMessage());
        state.setStatus(orderStatusWebhook.getNew_state());
        state.setMessage(orderStatusWebhook.getMessage());
        state.setDate(Utils.convertUnixTimeToDate(orderStatusWebhook.getTimestamp_unix()));
        orderStates.add(state);
        List<OrderState> mergeList = Stream.concat(order.getOrderStates().stream(), orderStates.stream()).collect(Collectors.toList());
        order.setOrderStates(mergeList);
        return order;
    }

    public static RiderDetails convertUrbanRiderToOrderRider(RiderWebhook riderWebhook) {
        ModelMapper mapper = new ModelMapper();
        List<DeliveryStatus> deliveryStatus = new ArrayList<>();
        RiderDetails details = new RiderDetails();
        DeliveryInfo delInfo = riderWebhook.getDelivery_info();
        details.setStatus(delInfo.getCurrent_state());
        details.setDateTime(Utils.getDate());
        details.setDeliveryPerson(mapper.map(delInfo.getDelivery_person_details(), DeliveryPerson.class));
        delInfo.getStatus_updates().forEach(stat -> {
            DeliveryStatus status = new DeliveryStatus();
            status.setComments(stat.getComments());
            status.setStatus(stat.getStatus());
            status.setCreated(Utils.convertUnixTimeToDate(stat.getCreated()));
            deliveryStatus.add(status);
        });
        details.setDeliveryStatus(deliveryStatus);
        return details;
    }

    public static StoreUrban convertStoreToStoreUrban(List<Store> stores, StoreUrban storeUrban) {
        List<com.tbi.core.urban.models.Store> storeList = new ArrayList<>();
        com.tbi.core.urban.models.Store uStore = new com.tbi.core.urban.models.Store();
        List<Timings> timings = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (Store store : stores) {
            uStore.setCity(store.getCity());
            uStore.setName(store.getName());
            uStore.setActive(store.isActive());
            uStore.setMin_pickup_time(store.getPickupTime());
            uStore.setMin_delivery_time(store.getDeliveryTime());
            uStore.setAddress(store.getAddress());
            uStore.setContact_phone(store.getContact());
            uStore.setRef_id(store.getRefId());
            uStore.setMin_order_value(store.getOrderValue());
            uStore.setHide_from_ui(store.getHideStatus());
            uStore.setOrdering_enabled(store.getOrderStatus());
            uStore.setGeo_longitude(store.getGeo_longitude());
            uStore.setGeo_latitude(store.getGeo_latitude());
            uStore.setNotification_emails(store.getEmails());
            uStore.setNotification_phones(store.getPhones());
            if (store.getPlatforms() != null) {
                uStore.setExcluded_platforms(store.getPlatforms());
            } else {
                uStore.setExcluded_platforms(new ArrayList<String>());
            }
            uStore.setZip_codes(store.getZipCodes());
            for (com.tbi.core.models.Timings time : store.getTimings()) {
                timings.add(mapper.map(time, Timings.class));
            }
            uStore.setTimings(timings);
            storeList.add(uStore);
        }
        storeUrban.setStores(storeList);
        return storeUrban;
    }

    public static StoreAction convertStoreActionToUrbanStoreAction(Store store, StoreAction storeUrban) {
        storeUrban.setLocation_ref_id(String.valueOf(store.getId()));
        storeUrban.setAction(store.getAction());
        storeUrban.setPlatforms(store.getPlatforms());
        return storeUrban;
    }

    public static com.tbi.core.urban.models.Catalogue convertCatalogueToUrbanCatalogue(Catalogue catalogue) {
        com.tbi.core.urban.models.Catalogue urbanCatalogue = new com.tbi.core.urban.models.Catalogue();
        ModelMapper modelMapper = new ModelMapper();
        convertItemToUrbanItem(catalogue, modelMapper, urbanCatalogue);
        urbanCatalogue.setFlush_items(catalogue.getFlush_items());
        urbanCatalogue.setFlush_options(catalogue.getFlush_options());
        return urbanCatalogue;
    }

    private static void convertItemToUrbanItem(Catalogue catalogue, ModelMapper modelMapper, com.tbi.core.urban.models.Catalogue urbanCatalogue) {
        List<Item> itemListUrban = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        List<String> tax_item_ref_ids = new ArrayList<>();
        List<String> charge_item_ref_ids = new ArrayList<>();
        List<OptionGroups> optionGroups = new ArrayList<>();
        List<Option> optionList = new ArrayList<>();
        Map<String, com.tbi.core.urban.webhook.models.Tax> taxMap = new HashMap<>();
        Map<String, Charge> chargeMap = new HashMap<>();
        catalogue.getItems().stream().forEach(item -> {
            List<String> category_ref_ids = new ArrayList<>();
            Item itemUrban = new Item();
            itemUrban.setTitle(item.getName());
            itemUrban.setAvailable(item.getAvailable());
            itemUrban.setCurrent_stock(item.getCurrentStock());
            itemUrban.setFood_type(item.getFoodType());
            itemUrban.setDescription(item.getDescription());
            itemUrban.setPrice(item.getPrice());
            itemUrban.setRef_id(item.getRefId());
            itemUrban.setSold_at_store(item.getSold_at_store());
            itemUrban.setRecommended(item.getRecommended());
            if (item.getTags() != null) {
                itemUrban.setTags(modelMapper.map(item.getTags(), Tags.class));
            } else {
                Tags tags = new Tags();
                tags.setSwiggy(new ArrayList<>());
                tags.setZomato(new ArrayList<>());
                itemUrban.setTags(tags);
            }
            category_ref_ids = item.getCategories().stream().map(p -> p.getRef_id()).collect(Collectors.toList());
            itemUrban.setCategory_ref_ids(category_ref_ids);
            itemUrban.setExcluded_platforms(new ArrayList<>());
            Type urbanCat = new TypeToken<List<Category>>() {
            }.getType();
            categories.addAll(modelMapper.map(item.getCategories(), urbanCat));
            if (item.getTaxes() != null && !item.getTaxes().isEmpty()) {
                tax_item_ref_ids.add(item.getRefId());
                convertTaxToUrbanTax(item, modelMapper, taxMap);
            } else {
                urbanCatalogue.setTaxes(new ArrayList<com.tbi.core.urban.webhook.models.Tax>());
            }
            if (item.getCharges() != null && !item.getCharges().isEmpty()) {
                charge_item_ref_ids.add(item.getRefId());
                convertChargeToUrbanCharge(item, modelMapper, chargeMap);
            } else {
                urbanCatalogue.setCharges(new ArrayList<com.tbi.core.urban.webhook.models.Charge>());
            }
            if (item.getOptionGroup() != null && !item.getOptionGroup().isEmpty()) {
                convertOptionGroupToUrbanOptionGroup(item, modelMapper, optionGroups, optionList);
            } else {
                urbanCatalogue.setOption_groups(new ArrayList<OptionGroups>());
                urbanCatalogue.setOptions(new ArrayList<Option>());
            }
            itemListUrban.add(itemUrban);
        });
        if (taxMap != null && !taxMap.isEmpty()) {
            urbanCatalogue.setTaxes(setTaxesInUPObject(taxMap, tax_item_ref_ids));
        }
        if (chargeMap != null && !chargeMap.isEmpty()) {
            urbanCatalogue.setCharges(setChargesInUPObject(chargeMap, charge_item_ref_ids));
        }
        urbanCatalogue.setOptions(optionList);
        urbanCatalogue.setOption_groups(optionGroups);
        Set<Category> categoryList = new LinkedHashSet<>(categories);
        categories.clear();
        categories.addAll(categoryList);
        urbanCatalogue.setCategories(categories);
        urbanCatalogue.setItems(itemListUrban);
    }

    private static List<Charge> setChargesInUPObject(Map<String, Charge> chargeMap, List<String> charge_item_ref_ids) {
        List<Charge> charges = new ArrayList<>();
        for (Entry<String, Charge> entry : chargeMap.entrySet()) {
            Charge charge = entry.getValue();
            charge.setItem_ref_ids(charge_item_ref_ids);
            charges.add(charge);
        }
        return charges;
    }

    private static List<com.tbi.core.urban.webhook.models.Tax> setTaxesInUPObject(Map<String, com.tbi.core.urban.webhook.models.Tax> taxMap,
            List<String> item_ref_ids) {
        List<com.tbi.core.urban.webhook.models.Tax> taxs = new ArrayList<>();
        for (Entry<String, com.tbi.core.urban.webhook.models.Tax> entry : taxMap.entrySet()) {
            com.tbi.core.urban.webhook.models.Tax tax = entry.getValue();
            tax.setItem_ref_ids(item_ref_ids);
            taxs.add(tax);
        }
        return taxs;
    }

    private static void convertOptionGroupToUrbanOptionGroup(com.tbi.core.models.Item item, ModelMapper modelMapper, List<OptionGroups> optionGroups,
            List<Option> optionList) {
        item.getOptionGroup().stream().forEach(group -> {
            OptionGroups urbanGroup = new OptionGroups();
            List<String> refIds = new ArrayList<>();
            urbanGroup.setActive(group.getActive());
            urbanGroup.setTitle(group.getName());
            urbanGroup.setRef_id(group.getRefId());
            urbanGroup.setMax_selectable(group.getMax_selectable());
            urbanGroup.setMin_selectable(group.getMin_selectable());
            convertOptiontourbanOption(group, modelMapper, optionList);
            refIds.add(item.getRefId());
            urbanGroup.setItem_ref_ids(refIds);
            optionGroups.add(urbanGroup);
        });

    }

    private static void convertOptiontourbanOption(OptionGroup group, ModelMapper modelMapper, List<Option> optionList) {
        if (group.getOptions() != null && !group.getOptions().isEmpty()) {
            group.getOptions().stream().forEach(option -> {
                List<String> refIds = new ArrayList<>();
                Option urbanOption = new Option();
                urbanOption.setAvailable(option.getAvailable());
                urbanOption.setDescription(option.getDescription());
                urbanOption.setPrice(option.getPrice());
                urbanOption.setRef_id(option.getRef_id());
                urbanOption.setSold_at_store(option.getSold_at_store());
                urbanOption.setTitle(option.getName());
                urbanOption.setWeight(option.getWeight());
                urbanOption.setFoodType(option.getFoodType());
                if (urbanOption.getNested_opt_grps() == null) {
                    urbanOption.setNested_opt_grps(new ArrayList<>());
                }
                refIds.add(group.getRefId());
                urbanOption.setOpt_grp_ref_ids(refIds);
                optionList.add(urbanOption);
            });
        }
    }

    private static void convertTaxToUrbanTax(com.tbi.core.models.Item item, ModelMapper modelMapper,
            Map<String, com.tbi.core.urban.webhook.models.Tax> taxMap) {
        item.getTaxes().stream().forEach(tax -> {
            if (!taxMap.containsKey(tax.getRef_id())) {
                com.tbi.core.urban.webhook.models.Tax urbanTax = new com.tbi.core.urban.webhook.models.Tax();
                urbanTax.setActive(tax.isActive());
                urbanTax.setTitle(tax.getName());
                urbanTax.setRef_id(tax.getRef_id());
                if(tax.getName().startsWith("SGST")) {
                    urbanTax.setCode("SGST_P"); 
                }else {
                    urbanTax.setCode("CGST_P"); 
                }
                urbanTax.setStructure(modelMapper.map(tax.getStructure(), Structure.class));
                taxMap.put(tax.getRef_id(), urbanTax);
            }
        });
    }

    private static void convertChargeToUrbanCharge(com.tbi.core.models.Item item, ModelMapper modelMapper, Map<String, Charge> chargeMap) {
        item.getCharges().stream().forEach(charge -> {
            if (!chargeMap.containsKey(charge.getRef_id())) {
                Charge urbanCharge = new Charge();
                urbanCharge.setActive(charge.getActive());
                urbanCharge.setDescription(charge.getDescription());
                urbanCharge.setTitle(charge.getName());
                urbanCharge.setRef_id(charge.getRef_id());
                if (urbanCharge.getExcluded_platforms() != null) {
                    urbanCharge.setExcluded_platforms(charge.getPlatforms());
                } else {
                    urbanCharge.setExcluded_platforms(new ArrayList<String>());
                }
                if (urbanCharge.getFulfillment_modes() != null) {
                    urbanCharge.setFulfillment_modes(charge.getFulfillment_modes());
                } else {
                    urbanCharge.setFulfillment_modes(new ArrayList<String>());
                }
                urbanCharge.setStructure(modelMapper.map(charge.getStructure(), Structure.class));
            }
        });
    }

    public static ItemAction convertItemActionToUrbanItemAction(ItemAction itemAction, Catalogue catalogue, String storeId) {
        List<String> item_ref_ids = new ArrayList<>();
        item_ref_ids = catalogue.getItems().stream().map(p -> p.getRefId()).collect(Collectors.toList());
        itemAction.setItem_ref_ids(item_ref_ids);
        itemAction.setLocation_ref_id(storeId);
        itemAction.setAction(catalogue.getAction());
        itemAction.setPlatforms(catalogue.getPlatforms());
        return itemAction;
    }

    public static ItemActionStatusUpdate setUPItemActionToItem(ItemActionCallback itemActionCallback) {
        ItemActionStatusUpdate actionStatusUpdate = new ItemActionStatusUpdate();
        List<ItemActionCallbackStatus> urbanItemActionCallbackList = itemActionCallback.getStatus();
        actionStatusUpdate.setAction(itemActionCallback.getAction());
        actionStatusUpdate.setPlatform(itemActionCallback.getPlatform());
        List<ItemActionSummary> actionSummaries = new ArrayList<>();
        for (ItemActionCallbackStatus urbanItemStatus : urbanItemActionCallbackList) {
            for (com.tbi.core.urban.webhook.models.Item item : urbanItemStatus.getItems()) {
                ItemActionSummary summary = new ItemActionSummary();
                Location location = urbanItemStatus.getLocation();
                summary.setItem_refId(item.getRef_id());
                summary.setStore_RefId(location.getRef_id());
                summary.setStatus(item.getStatus());
                actionSummaries.add(summary);
            }
        }
        actionStatusUpdate.setItemActionSummaries(actionSummaries);
        return actionStatusUpdate;
    }

    public static ThirdPartyResponseStatus convertStoreCreationResponseToTbi(StoreCreationWebhookResponse store, String type) {
        ThirdPartyResponseStatus partyResponseStatus = new ThirdPartyResponseStatus();
        Stats webhookResponse = store.getStats();
        partyResponseStatus.setCreated(webhookResponse.getCreated());
        partyResponseStatus.setErrors(webhookResponse.getErrors());
        partyResponseStatus.setAction(type);
        partyResponseStatus.setUpdated(webhookResponse.getUpdated());
        return partyResponseStatus;
    }

    public static List<ThirdPartyResponseStatus> convertWebhookResponseToEre4uResponse(CatalogueWebhook catalogueWebhook) {
        
        List<ThirdPartyResponseStatus> responseList = new ArrayList<ThirdPartyResponseStatus>();
        
        Stats webhookResponse = catalogueWebhook.getStats();
        
        com.tbi.core.urban.webhook.models.Category category = webhookResponse.getCategories();
        ThirdPartyResponseStatus partyResponseStatus2 = new ThirdPartyResponseStatus();
        partyResponseStatus2.setCreated(category.getCreated());
        partyResponseStatus2.setErrors(category.getErrors());
        partyResponseStatus2.setAction("Category :");
        partyResponseStatus2.setUpdated(category.getUpdated());
        responseList.add(partyResponseStatus2);
        
        com.tbi.core.urban.webhook.models.Item item = webhookResponse.getItems();
        partyResponseStatus2 = new ThirdPartyResponseStatus();
        partyResponseStatus2.setCreated(item.getCreated());
        partyResponseStatus2.setErrors(item.getErrors());
        partyResponseStatus2.setAction("Item :");
        partyResponseStatus2.setUpdated(item.getUpdated());
        responseList.add(partyResponseStatus2);
        
        com.tbi.core.urban.webhook.models.OptionGroups group = webhookResponse.getOption_groups();
        partyResponseStatus2 = new ThirdPartyResponseStatus();
        partyResponseStatus2.setCreated(group.getCreated());
        partyResponseStatus2.setErrors(group.getErrors());
        partyResponseStatus2.setAction("Option Group :");
        partyResponseStatus2.setUpdated(group.getUpdated());
        responseList.add(partyResponseStatus2);
        
        com.tbi.core.urban.webhook.models.Option option = webhookResponse.getOptions();
        partyResponseStatus2 = new ThirdPartyResponseStatus();
        partyResponseStatus2.setCreated(option.getCreated());
        partyResponseStatus2.setErrors(option.getErrors());
        partyResponseStatus2.setAction("Option :");
        partyResponseStatus2.setUpdated(option.getUpdated());
        responseList.add(partyResponseStatus2);
        
        return responseList;
    }

}
