/**
 * 
 */
package com.tbi.core.urban.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class ItemAction {
	
	private String location_ref_id;
	private List<String> platforms;
	private List<String> item_ref_ids;
	private String action;
	
	
	public String getLocation_ref_id() {
		return location_ref_id;
	}
	public void setLocation_ref_id(String location_ref_id) {
		this.location_ref_id = location_ref_id;
	}
	public List<String> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<String> platforms) {
		this.platforms = platforms;
	}
	public List<String> getItem_ref_ids() {
		return item_ref_ids;
	}
	public void setItem_ref_ids(List<String> item_ref_ids) {
		this.item_ref_ids = item_ref_ids;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
}
