/**
 * 
 */
package com.tbi.core.urban.models;

/**
 * @author shameem
 *
 */
public class OrderStatus {
	
	private String new_status;
	private String message;
	
	public String getNew_status() {
		return new_status;
	}
	public void setNew_status(String new_status) {
		this.new_status = new_status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
