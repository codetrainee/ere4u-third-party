/**
 * 
 */
package com.tbi.core.urban.models;

/**
 * @author shameem
 *
 */
public class Category {

	private Boolean active;
	private String description;
	private String img_url;
	private String name;
	private String ref_id;
	private Integer sort_order;
	private String parent_ref_id;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRef_id() {
		return ref_id;
	}

	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public Integer getSort_order() {
		return sort_order;
	}

	public void setSort_order(Integer sort_order) {
		this.sort_order = sort_order;
	}

    /**
     * @return the parent_ref_id
     */
    public String getParent_ref_id() {
        return parent_ref_id;
    }

    /**
     * @param parent_ref_id the parent_ref_id to set
     */
    public void setParent_ref_id(String parent_ref_id) {
        this.parent_ref_id = parent_ref_id;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((img_url == null) ? 0 : img_url.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent_ref_id == null) ? 0 : parent_ref_id.hashCode());
        result = prime * result + ((ref_id == null) ? 0 : ref_id.hashCode());
        result = prime * result + ((sort_order == null) ? 0 : sort_order.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Category other = (Category) obj;
        if (active == null) {
            if (other.active != null)
                return false;
        } else if (!active.equals(other.active))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (img_url == null) {
            if (other.img_url != null)
                return false;
        } else if (!img_url.equals(other.img_url))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parent_ref_id == null) {
            if (other.parent_ref_id != null)
                return false;
        } else if (!parent_ref_id.equals(other.parent_ref_id))
            return false;
        if (ref_id == null) {
            if (other.ref_id != null)
                return false;
        } else if (!ref_id.equals(other.ref_id))
            return false;
        if (sort_order == null) {
            if (other.sort_order != null)
                return false;
        } else if (!sort_order.equals(other.sort_order))
            return false;
        return true;
    }
	     
}
