/**
 * 
 */
package com.tbi.core.urban.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class StoreAction {
	
	private String location_ref_id;
	private List<String> platforms;
	private String action;
	
	public String getLocation_ref_id() {
		return location_ref_id;
	}
	public void setLocation_ref_id(String location_ref_id) {
		this.location_ref_id = location_ref_id;
	}
	public List<String> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<String> platforms) {
		this.platforms = platforms;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	
}
