/**
 * 
 */
package com.tbi.core.urban.models;

import java.util.List;

import com.tbi.core.urban.webhook.models.Timings;

/**
 * @author shameem
 *
 */
public class Store {

    private String city;
    private String name;
    private Integer min_pickup_time;
    private Integer min_delivery_time;
    private String contact_phone;
    private String ref_id;
    private Integer min_order_value;
    private Boolean hide_from_ui;
    private String address;
    private Double geo_longitude;
    private Boolean active;
    private Double geo_latitude;
    private Boolean ordering_enabled;
    private List<String> notification_emails;
    private List<String> zip_codes;
    private List<String> notification_phones;
    private List<String> excluded_platforms;
    private List<Timings> timings;
    private String merchant_ref_id;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMin_pickup_time() {
        return min_pickup_time;
    }

    public void setMin_pickup_time(Integer min_pickup_time) {
        this.min_pickup_time = min_pickup_time;
    }

    public Integer getMin_delivery_time() {
        return min_delivery_time;
    }

    public void setMin_delivery_time(Integer min_delivery_time) {
        this.min_delivery_time = min_delivery_time;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public List<String> getNotification_phones() {
        return notification_phones;
    }

    public void setNotification_phones(List<String> notification_phones) {
        this.notification_phones = notification_phones;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public Integer getMin_order_value() {
        return min_order_value;
    }

    public void setMin_order_value(Integer min_order_value) {
        this.min_order_value = min_order_value;
    }

    public Boolean getHide_from_ui() {
        return hide_from_ui;
    }

    public void setHide_from_ui(Boolean hide_from_ui) {
        this.hide_from_ui = hide_from_ui;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getNotification_emails() {
        return notification_emails;
    }

    public void setNotification_emails(List<String> notification_emails) {
        this.notification_emails = notification_emails;
    }

    public List<String> getZip_codes() {
        return zip_codes;
    }

    public void setZip_codes(List<String> zip_codes) {
        this.zip_codes = zip_codes;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getOrdering_enabled() {
        return ordering_enabled;
    }

    public void setOrdering_enabled(Boolean ordering_enabled) {
        this.ordering_enabled = ordering_enabled;
    }

    public List<String> getExcluded_platforms() {
        return excluded_platforms;
    }

    public void setExcluded_platforms(List<String> excluded_platforms) {
        this.excluded_platforms = excluded_platforms;
    }

    public List<Timings> getTimings() {
        return timings;
    }

    public void setTimings(List<Timings> timings) {
        this.timings = timings;
    }

    /**
     * @return the merchant_ref_id
     */
    public String getMerchant_ref_id() {
        return merchant_ref_id;
    }

    /**
     * @param merchant_ref_id the merchant_ref_id to set
     */
    public void setMerchant_ref_id(String merchant_ref_id) {
        this.merchant_ref_id = merchant_ref_id;
    }

    /**
     * @return the geo_longitude
     */
    public Double getGeo_longitude() {
        return geo_longitude;
    }

    /**
     * @return the geo_latitude
     */
    public Double getGeo_latitude() {
        return geo_latitude;
    }

    /**
     * @param geo_longitude the geo_longitude to set
     */
    public void setGeo_longitude(Double geo_longitude) {
        this.geo_longitude = geo_longitude;
    }

    /**
     * @param geo_latitude the geo_latitude to set
     */
    public void setGeo_latitude(Double geo_latitude) {
        this.geo_latitude = geo_latitude;
    }
    
    
}
