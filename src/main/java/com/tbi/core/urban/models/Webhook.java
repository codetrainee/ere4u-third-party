/**
 * 
 */
package com.tbi.core.urban.models;

/**
 * @author shameem
 *
 */
public class Webhook {
	
	private boolean active;
	private String event_type;
	private String retrial_interval_units;
	private String url;
	
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getEvent_type() {
		return event_type;
	}
	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}
	public String getRetrial_interval_units() {
		return retrial_interval_units;
	}
	public void setRetrial_interval_units(String retrial_interval_units) {
		this.retrial_interval_units = retrial_interval_units;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
