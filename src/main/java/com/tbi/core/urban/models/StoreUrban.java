/**
 * 
 */
package com.tbi.core.urban.models;

import java.util.List;

/**
 * @author shameem
 *
 */
public class StoreUrban {

    private List<com.tbi.core.urban.models.Store> stores;

    /**
     * @return the stores
     */
    public List<com.tbi.core.urban.models.Store> getStores() {
        return stores;
    }

    /**
     * @param storeList the stores to set
     */
    public void setStores(List<com.tbi.core.urban.models.Store> storeList) {
        this.stores = storeList;
    }
    
    
}
