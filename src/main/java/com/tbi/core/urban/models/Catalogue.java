/**
 * 
 */
package com.tbi.core.urban.models;

import java.util.List;


import com.tbi.core.urban.webhook.models.Charge;

import com.tbi.core.urban.webhook.models.Tax;

/**
 * @author shameem
 *
 */

public class Catalogue {
    
    private List<Category> categories;
    private List<Charge> charges;
    private List<Tax> taxes;
    private List<Item> items;
    private List<OptionGroups> option_groups;
    private List<Option> options;
    private Boolean flush_items;
    private Boolean flush_options;
  
    /**
     * @return the categories
     */
    public List<Category> getCategories() {
        return categories;
    }
    /**
     * @param categories the categories to set
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
    /**
     * @return the charges
     */
    public List<Charge> getCharges() {
        return charges;
    }
    /**
     * @param charges the charges to set
     */
    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }
    /**
     * @return the flush_items
     */
    public Boolean getFlush_items() {
        return flush_items;
    }
    /**
     * @param flush_items the flush_items to set
     */
    public void setFlush_items(Boolean flush_items) {
        this.flush_items = flush_items;
    }
    /**
     * @return the flush_options
     */
    public Boolean getFlush_options() {
        return flush_options;
    }
    /**
     * @param flush_options the flush_options to set
     */
    public void setFlush_options(Boolean flush_options) {
        this.flush_options = flush_options;
    }
    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }
    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }
    /**
     * @return the option_groups
     */
    public List<OptionGroups> getOption_groups() {
        return option_groups;
    }
    /**
     * @param option_groups the option_groups to set
     */
    public void setOption_groups(List<OptionGroups> option_groups) {
        this.option_groups = option_groups;
    }
    /**
     * @return the options
     */
    public List<Option> getOptions() {
        return options;
    }
    /**
     * @param options the options to set
     */
    public void setOptions(List<Option> options) {
        this.options = options;
    }
    /**
     * @return the taxes
     */
    public List<Tax> getTaxes() {
        return taxes;
    }
    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }
}
