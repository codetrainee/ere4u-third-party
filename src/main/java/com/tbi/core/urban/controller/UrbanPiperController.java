/**
 * 
 */
package com.tbi.core.urban.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.ItemActionStatusUpdate;
import com.tbi.core.urban.service.OrderUrbanService;
import com.tbi.core.urban.webhook.models.CatalogueWebhook;
import com.tbi.core.urban.webhook.models.ItemActionCallback;
import com.tbi.core.urban.webhook.models.OrderStatusUpdateWebhookResponse;
import com.tbi.core.urban.webhook.models.OrderWebhook;
import com.tbi.core.urban.webhook.models.RiderWebhook;
import com.tbi.core.urban.webhook.models.StoreActionWebhooksResponse;
import com.tbi.core.urban.webhook.models.StoreCreationWebhookResponse;

/**
 * @author shameem
 *
 */
@RestController
@RequestMapping("/urban-piper/endpoints/")
public class UrbanPiperController {

    @Autowired
    OrderUrbanService orderUrbanService;
   
    @PostMapping("store")
    public StoreCreationWebhookResponse storeWebhookCallback(@Valid @RequestBody StoreCreationWebhookResponse store) throws Exception {
        orderUrbanService.updateUrbanStoreConfigWebhookResponse(store);
        return store;
    }
    
    @PostMapping("store/action")
    public StoreActionWebhooksResponse storeActionWebhookCallback(@RequestBody StoreActionWebhooksResponse storeActionCallback) throws Exception {
        orderUrbanService.updateStoreActionWebhookRes(storeActionCallback);
        return storeActionCallback;
    }
    
    
    @PostMapping("item/action")
    public ItemActionStatusUpdate itemActionCallbackUpdate(@RequestBody ItemActionCallback itemActionCallback) throws Exception {
        return orderUrbanService.itemActionCallbackUpdate(itemActionCallback);
    }

    @PostMapping("order")
    public void thirdPartyOrder(@RequestBody OrderWebhook orderWebhook) {
        orderUrbanService.createNewOrder(orderWebhook);
    }
    
    @PostMapping("order/status")
    public void orderSatusChangeWebhookCallback(@RequestBody OrderStatusUpdateWebhookResponse orderStatusWebhook) {
        orderUrbanService.statusUpdateOfUrbanOrder(orderStatusWebhook);
    }

    @PostMapping("rider/status")
    public void updateRiderStatus(@RequestBody RiderWebhook riderWebhook) {
        orderUrbanService.statusUpdateOfUrbanRider(riderWebhook);
    }
    
    @PostMapping("/catalogue")
    public CatalogueWebhook catalogueWebhookCallback(@RequestBody CatalogueWebhook catalogueWebhook) {
        return orderUrbanService.updateCatalogueWebhookResponse(catalogueWebhook);
    }

}
