package com.tbi.core.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment_type")
public class PaymentType {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String paymentType;
	private String paymentTypeId;

//	@ManyToOne(cascade = { CascadeType.ALL}, fetch = FetchType.LAZY, targetEntity = PaymentType.class)
//	@JoinColumn(name = "pincode_id", referencedColumnName = "id")
//	private Pincode pincode;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

//	public Pincode getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(Pincode pincode) {
//		this.pincode = pincode;
//	}

	
}
