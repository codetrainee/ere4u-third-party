package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class OptionsToAdd {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer s_id;
	private Integer id;
	private String merchant_id;
	private Float price;
	private String title;
	private String quantity;

	/*
	 * @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=
	 * OptionGroup.class,orphanRemoval=true)
	 * 
	 * @JoinColumn(name = "optionGroupId", referencedColumnName="id") private
	 * List<OptionGroup> optionGroup;
	 * 
	 * 
	 *//**
		 * @return the optionGroup
		 */
	/*
	 * public List<OptionGroup> getOptionGroup() { return optionGroup; }
	 *//**
		 * @param optionGroup the optionGroup to set
		 *//*
			 * public void setOptionGroup(List<OptionGroup> optionGroup) { this.optionGroup
			 * = optionGroup; }
			 */

	/**
	 * @return the s_id
	 */
	public Integer getS_id() {
		return s_id;
	}

	/**
	 * @param s_id the s_id to set
	 */
	public void setS_id(Integer s_id) {
		this.s_id = s_id;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the merchant_id
	 */
	public String getMerchant_id() {
		return merchant_id;
	}

	/**
	 * @param merchant_id the merchant_id to set
	 */
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	/**
	 * @return the price
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	

}
