package com.tbi.core.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/* Author Shrikant Mishra
	Date ==>7/07/2020
*/

@Entity
@Table(name="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	
	private String fristName;
	private String lastName;
	private String dob;
	private Long mobileNo;
	private Long contactNo1;
	private String status;
	private Integer clientId;
	private String email;
	private String profileImage;	
	private String pincode;
		
	@Column(name="username",updatable= false)
	private String username;
	
	@Column(name="password")
	private String password;
	
	private Date createdDate;
	
	@OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY,targetEntity =AddressUser.class)
    @JoinColumn(name="userId",referencedColumnName="userId")
    private List<AddressUser> addressUser;
	
	@OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY,targetEntity =Customer.class)
    @JoinColumn(name="userId",referencedColumnName="userId")
    private List<Customer> customer;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch= FetchType.EAGER)
    @JoinColumn(name="cityId",referencedColumnName="id")
    private City city;
	
	@ManyToOne(cascade = CascadeType.REMOVE,fetch= FetchType.EAGER)
    @JoinColumn(name="stateId",referencedColumnName="id")
    private State state;
	
	
	
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	public List<Customer> getCustomer() {
		return customer;
	}

	public void setCustomer(List<Customer> customer) {
		this.customer = customer;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public Long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Long getContactNo1() {
		return contactNo1;
	}

	public void setContactNo1(Long contactNo1) {
		this.contactNo1 = contactNo1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<AddressUser> getAddressUser() {
		return addressUser;
	}

	public void setAddressUser(List<AddressUser> addressUser) {
		this.addressUser = addressUser;
	}

	
}
