/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class ThirdPartyMapping {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Integer thirdPartyId;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the thirdPartyId
     */
    public Integer getThirdPartyId() {
        return thirdPartyId;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param thirdPartyId the thirdPartyId to set
     */
    public void setThirdPartyId(Integer thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }
    
    
    
}
