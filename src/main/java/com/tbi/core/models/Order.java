/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Order {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String currentState;
    private Integer storeId;
    private String message;
    private Date currentDate;
    private String channel; 
    private String previousState;
    private String provider;
    @GeneratedValue(generator="refId")
    @SequenceGenerator(name="refId",sequenceName="MY_SEQ", allocationSize=1)
    private Integer refId;
    private Long userId;
    @Transient
    private User user;
   
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY,targetEntity =OrderState.class)
    @JoinColumn(name="orderId",referencedColumnName="id")
    private List<OrderState> orderStates;
    
    @OneToOne(targetEntity= Customer.class,cascade = CascadeType.ALL)
    @JoinColumn(name="customerId",referencedColumnName="id")
    private Customer customerDetails;
    
    @OneToOne(targetEntity= ExternalChannalInfo.class,cascade=CascadeType.ALL )
    @JoinColumn(name="extChannelId",referencedColumnName="id")
    private ExternalChannalInfo extChannel;
   
    @OneToOne(targetEntity= OrderDetails.class,cascade= CascadeType.ALL )
    @JoinColumn(name="detailsId",referencedColumnName="id")
    private OrderDetails details;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = OrderItems.class)
   // @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH }, targetEntity = OrderItems.class)
    @JoinColumn(name = "orderId", referencedColumnName = "id")
    private List<OrderItems> items;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = OrderPayments.class)
    @JoinColumn(name="orderId",referencedColumnName="id")
    private List<OrderPayments> payment;
    
    @OneToOne(targetEntity= RiderDetails.class,cascade= CascadeType.ALL )
    @JoinColumn(name="riderId",referencedColumnName="id")
    private RiderDetails riderDetails;

	public Customer getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(Customer customerDetails) {
		this.customerDetails = customerDetails;
	}
	

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the currentState
     */
    public String getCurrentState() {
        return currentState;
    }

    /**
     * @return the previousState
     */
    public String getPreviousState() {
        return previousState;
    }

    /**
     * @param currentState the currentState to set
     */
    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    /**
     * @param previousState the previousState to set
     */
    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    /**
     * @return the date
     */
  

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }


 

    /**
     * @return the storeId
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * @param storeId the storeId to set
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the details
     */
    public OrderDetails getDetails() {
        return details;
    }

    /**
     * @return the items
     */
    public List<OrderItems> getItems() {
        return items;
    }

  

    /**
     * @return the payment
     */
    public List<OrderPayments> getPayment() {
        return payment;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param details
     *            the details to set
     */
    public void setDetails(OrderDetails details) {
        this.details = details;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(List<OrderItems> items) {
        this.items = items;
    }

    /**
     * @param payment
     *            the payment to set
     */
    public void setPayment(List<OrderPayments> payment) {
        this.payment = payment;
    }

    /**
     * @return the extChannel
     */
    public ExternalChannalInfo getExtChannel() {
        return extChannel;
    }

    /**
     * @param extChannel the extChannel to set
     */
    public void setExtChannel(ExternalChannalInfo extChannel) {
        this.extChannel = extChannel;
    }

    
    /**
     * @return the riderDetails
     */
    public RiderDetails getRiderDetails() {
        return riderDetails;
    }

  

    /**
     * @param riderDetails the riderDetails to set
     */
    public void setRiderDetails(RiderDetails riderDetails) {
        this.riderDetails = riderDetails;
    }

    /**
     * @return the orderStates
     */
    public List<OrderState> getOrderStates() {
        return orderStates;
    }

    /**
     * @param orderStates the orderStates to set
     */
    public void setOrderStates(List<OrderState> orderStates) {
        this.orderStates = orderStates;
    }

	public Integer getRefId() {
		return refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

    /**
     * @return the refId
     */
   

   
    
}
