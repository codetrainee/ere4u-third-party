/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author shameem
 *
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
public class OrderState {
    
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String status;
    @CreatedDate
    private Date date;
    private String message;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
