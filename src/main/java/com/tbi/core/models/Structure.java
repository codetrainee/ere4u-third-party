/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class Structure {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String applicable_on;
    private String type;
    private Float value;
    
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the applicable_on
     */
    public String getApplicable_on() {
        return applicable_on;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
   
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param applicable_on the applicable_on to set
     */
    public void setApplicable_on(String applicable_on) {
        this.applicable_on = applicable_on;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }
    
}
