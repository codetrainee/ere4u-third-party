/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class Slots {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String start_time;
    private String end_time;
   
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the start_time
     */
    public String getStart_time() {
        return start_time;
    }
    /**
     * @return the end_time
     */
    public String getEnd_time() {
        return end_time;
    }
    /**
     * @param start_time the start_time to set
     */
    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }
    /**
     * @param end_time the end_time to set
     */
    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
    
    
}