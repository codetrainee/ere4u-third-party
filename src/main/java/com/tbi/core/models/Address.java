/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 * @author shameem
 *
 */
@Entity
public class Address {
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String city;
    private Boolean is_guest_mode;
    private String landmark;
    private Double latitude;
    private String line_1;
    private String line_2;
    private Double longitude;
    private String pin;
    private String sub_locality;
    private String tag;
    
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @return the is_guest_mode
     */
    public Boolean getIs_guest_mode() {
        return is_guest_mode;
    }
    /**
     * @param is_guest_mode the is_guest_mode to set
     */
    public void setIs_guest_mode(Boolean is_guest_mode) {
        this.is_guest_mode = is_guest_mode;
    }
    /**
     * @return the landmark
     */
    public String getLandmark() {
        return landmark;
    }
    /**
     * @param landmark the landmark to set
     */
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }
    /**
     * @return the line_1
     */
    public String getLine_1() {
        return line_1;
    }
    /**
     * @param line_1 the line_1 to set
     */
    public void setLine_1(String line_1) {
        this.line_1 = line_1;
    }
    /**
     * @return the line_2
     */
    public String getLine_2() {
        return line_2;
    }
    /**
     * @param line_2 the line_2 to set
     */
    public void setLine_2(String line_2) {
        this.line_2 = line_2;
    }
   
    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }
    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }
    /**
     * @return the sub_locality
     */
    public String getSub_locality() {
        return sub_locality;
    }
    /**
     * @param sub_locality the sub_locality to set
     */
    public void setSub_locality(String sub_locality) {
        this.sub_locality = sub_locality;
    }
    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }
    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }
    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }
    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }
    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
   
}
