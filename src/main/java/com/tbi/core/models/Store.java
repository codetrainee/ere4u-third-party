/**
 * 
 */
package com.tbi.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author shameem
 *
 */
@Entity
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String address;
    private String email;
    private String city;
    private String contact;
    private Boolean orderStatus;
    private Integer pickupTime;
    private Integer deliveryTime;
    private Integer orderValue;
    private Boolean hideStatus;
    private Double geo_latitude;
    private Double geo_longitude;
    private String action;
    private String provider;
    @NotBlank
    @Column(unique = true)
    private String refId;
    private boolean active;
    private String authKey;
    
    @ElementCollection 
    private List<String> emails;
    @ElementCollection 
    private List<String> phones;
    @ElementCollection 
    private List<String> zipCodes;
    @ElementCollection
    private List<String> platforms;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=Timings.class)
    @JoinColumn(name = "storeId", referencedColumnName="id")
    private List<Timings> timings;
   
    @OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=ThirdPartyResponseStatus.class)
    @JoinColumn(referencedColumnName="id")
    private ThirdPartyResponseStatus thirdPartyResponseStatus;
    
    @OneToOne(cascade=CascadeType.ALL,targetEntity=Catalogue.class)
    @JoinColumn(referencedColumnName="id")
    private Catalogue catalogue;
    @JsonIgnore
    @OneToMany(targetEntity=Pincode.class, mappedBy="store",cascade=CascadeType.ALL, fetch = FetchType.LAZY)    
    private List<Pincode> pincode = new ArrayList<>();
    
    
    
    public List<Pincode> getPincode() {
		return pincode;
	}
	public void setPincode(List<Pincode> pincode) {
		this.pincode = pincode;
	}
	/**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }
    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }
    /**
     * @param emails the emails to set
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
    /**
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }
    /**
     * @param phones the phones to set
     */
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }
   
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }
    /**
     * @return the orderStatus
     */
    public Boolean getOrderStatus() {
        return orderStatus;
    }
    
    /**
     * @return the orderValue
     */
    public Integer getOrderValue() {
        return orderValue;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
    
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }
    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(Boolean orderStatus) {
        this.orderStatus = orderStatus;
    }
    
   
    /**
     * @param orderValue the orderValue to set
     */
    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue;
    }
    
    /**
     * @return the hideStatus
     */
    public Boolean getHideStatus() {
        return hideStatus;
    }
    
    /**
     * @param hideStatus the hideStatus to set
     */
    public void setHideStatus(Boolean hideStatus) {
        this.hideStatus = hideStatus;
    }
    
    /**
     * @return the pickupTime
     */
    public Integer getPickupTime() {
        return pickupTime;
    }
    /**
     * @return the deliveryTime
     */
    public Integer getDeliveryTime() {
        return deliveryTime;
    }
    /**
     * @param pickupTime the pickupTime to set
     */
    public void setPickupTime(Integer pickupTime) {
        this.pickupTime = pickupTime;
    }
    /**
     * @param deliveryTime the deliveryTime to set
     */
    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
   
    /**
     * @return the zipCodes
     */
    public List<String> getZipCodes() {
        return zipCodes;
    }
    /**
     * @param zipCodes the zipCodes to set
     */
    public void setZipCodes(List<String> zipCodes) {
        this.zipCodes = zipCodes;
    }
    /**
     * @return the timings
     */
    public List<Timings> getTimings() {
        return timings;
    }
    /**
     * @param timings the timings to set
     */
    public void setTimings(List<Timings> timings) {
        this.timings = timings;
    }
    /**
     * @return the platforms
     */
    public List<String> getPlatforms() {
        return platforms;
    }
    /**
     * @param platforms the platforms to set
     */
    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }
    
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }
    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }
    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @return the catalogue
     */
    public Catalogue getCatalogue() {
        return catalogue;
    }
    /**
     * @param catalogue the catalogue to set
     */
    public void setCatalogue(Catalogue catalogue) {
        this.catalogue = catalogue;
    }
    /**
     * @return the authKey
     */
    public String getAuthKey() {
        return authKey;
    }
    /**
     * @param authKey the authKey to set
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
    /**
     * @return the thirdPartyResponseStatus
     */
    public ThirdPartyResponseStatus getThirdPartyResponseStatus() {
        return thirdPartyResponseStatus;
    }
    /**
     * @param thirdPartyResponseStatus the thirdPartyResponseStatus to set
     */
    public void setThirdPartyResponseStatus(ThirdPartyResponseStatus thirdPartyResponseStatus) {
        this.thirdPartyResponseStatus = thirdPartyResponseStatus;
    }
    /**
     * @return the geo_latitude
     */
    public Double getGeo_latitude() {
        return geo_latitude;
    }
    /**
     * @return the geo_longitude
     */
    public Double getGeo_longitude() {
        return geo_longitude;
    }
    /**
     * @param geo_latitude the geo_latitude to set
     */
    public void setGeo_latitude(Double geo_latitude) {
        this.geo_latitude = geo_latitude;
    }
    /**
     * @param geo_longitude the geo_longitude to set
     */
    public void setGeo_longitude(Double geo_longitude) {
        this.geo_longitude = geo_longitude;
    }
    
    
}
