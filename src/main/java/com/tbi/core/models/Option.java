/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author shameem
 *
 */
@Entity
@Table(name="option")
@EntityListeners(AuditingEntityListener.class)
public class Option {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;
    private Boolean available;
    private Integer price;
    private Integer weight;
    private Boolean sold_at_store;
    @CreatedDate
    @Temporal(TemporalType.DATE)
    private Date date;
    private String ref_id;
    private String foodType;
   
    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }
    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }
    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }
    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
   
    /**
     * @return the sold_at_store
     */
    public Boolean getSold_at_store() {
        return sold_at_store;
    }
    /**
     * @param sold_at_store the sold_at_store to set
     */
    public void setSold_at_store(Boolean sold_at_store) {
        this.sold_at_store = sold_at_store;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @return the available
     */
    public Boolean getAvailable() {
        return available;
    }
    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }
    /**
     * @return the weight
     */
    public Integer getWeight() {
        return weight;
    }
 
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param available the available to set
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }
    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }
    /**
     * @param weight the weight to set
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }
    /**
     * @return the foodType
     */
    public String getFoodType() {
        return foodType;
    }
    /**
     * @param foodType the foodType to set
     */
    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }
    
    
}
