/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Charges {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String name;
    @CreatedDate
    private Date createDate;
    private Boolean sortOrder;
    private Boolean active;
    private String description;
    private String ref_id;
    private String deliveryCharge;
    @ElementCollection
    private List<String> platforms;
    
    @ElementCollection
    private List<String> fulfillment_modes;
    
    @OneToOne(targetEntity= Structure.class,cascade= CascadeType.ALL )
    @JoinColumn(name="structureId",referencedColumnName="id")
    private Structure structure;
    
    
    
   
    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }
    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }
    /**
     * @return the sortOrder
     */
    public Boolean getSortOrder() {
        return sortOrder;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(Boolean sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    /**
     * @return the platforms
     */
    public List<String> getPlatforms() {
        return platforms;
    }
    /**
     * @return the structure
     */
    public Structure getStructure() {
        return structure;
    }
  
    /**
     * @param platforms the platforms to set
     */
    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }
    /**
     * @param structure the structure to set
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }
   
    /**
     * @return the fulfillment_modes
     */
    public List<String> getFulfillment_modes() {
        return fulfillment_modes;
    }
    /**
     * @param fulfillment_modes the fulfillment_modes to set
     */
    public void setFulfillment_modes(List<String> fulfillment_modes) {
        this.fulfillment_modes = fulfillment_modes;
    }
	/**
	 * @return the deliveryCharge
	 */
	public String getDeliveryCharge() {
		return deliveryCharge;
	}
	/**
	 * @param deliveryCharge the deliveryCharge to set
	 */
	public void setDeliveryCharge(String deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}
    
    
    
    
}
