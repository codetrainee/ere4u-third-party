package com.tbi.core.models;

public class ResponseCode {
	
	public final static String OTP_VERIFIED_OK_CODE = "2001";

	public final static String OTP_VERIFIED_OK_CODE_MESSAGE = "OTP authenticated";
	
	public final static String OTP_EXPIRED_CODE = "2003";

	public final static String OTP_EXPIRED_CODE_MESSAGE = "OTP Expired";
	
	public final static String OTP_NOT_EXIST_CODE = "2002";

	public final static String OTP_NOT_EXIST_CODE_MESSAGE = "OTP Not Exist";
	
	public final static String STATUS_PENDING = "pending";
	
	public final static String SMS_DETAIL = "SEND_SMS";
	
	public final static String USER_EXISTS = "200";
	
	public final static String NOT_USER_EXISTS = "400";
	
	public final static String OTP_MSG= "is your OTP to log in to your web account. Happy Shopping! -TBiTEC";
	
	public final static String FORGATE_PWD_OTP = "is your Forgate Password OTP";
	
	public final static String GENERATE_ORDER_ID = "GENERATE_ORDER_ID";
	
	public final static String ALREADY_REGISTERED = "ALREADY REGISTERED 200";
	
	public final static String NOT_REGISTERED = "NOT REGISTERED 400";
	
	public final static String PINCODE_AND_STORE_ID_NOT_MATCHED = "PINCODE AND STORE ID NOT MATCHED";
	
	

}
