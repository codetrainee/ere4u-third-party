/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private String name;
    @NotBlank
    @Column(unique = true)
    private String code;
    @CreatedDate
    private Date CreateDate;
    private String status;
    private Integer credits;
    @LastModifiedDate
    private Date modifyDate;
    private Integer balance;
    private Integer creditDeduction;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ClientRecharge.class)
    @JoinColumn(name = "customerid", referencedColumnName = "id")
    private List<ClientRecharge> customerRechargs;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Store.class)
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private List<Store> stores;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date createDate) {
        CreateDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getCreditDeduction() {
        return creditDeduction;
    }

    public void setCreditDeduction(Integer creditDeduction) {
        this.creditDeduction = creditDeduction;
    }
    
    public List<ClientRecharge> getCustomerRechargs() {
        return customerRechargs;
    }

    public void setCustomerRechargs(List<ClientRecharge> customerRechargs) {
        this.customerRechargs = customerRechargs;
    }

    /**
     * @return the stores
     */
    public List<Store> getStores() {
        return stores;
    }

    /**
     * @param stores
     *            the stores to set
     */
    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

}
