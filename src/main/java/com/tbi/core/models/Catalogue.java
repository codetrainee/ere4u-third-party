/**
 * 
 */
package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * @author shameem
 *
 */
@Entity
public class Catalogue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Item.class)
    @JoinColumn(name = "clientCatlogueId", referencedColumnName = "id")
    private List<Item> items;

    // use for disable/enable particular items

    private String action;

    // use for disable/enable particular platforms

    @ElementCollection
    private List<String> platforms;

    private Boolean flush_items;

    private Boolean flush_options;

    /**
     * @return the flush_items
     */
    public Boolean getFlush_items() {
        return flush_items;
    }

    /**
     * @return the flush_options
     */
    public Boolean getFlush_options() {
        return flush_options;
    }

    /**
     * @param flush_items
     *            the flush_items to set
     */
    public void setFlush_items(Boolean flush_items) {
        this.flush_items = flush_items;
    }

    /**
     * @param flush_options
     *            the flush_options to set
     */
    public void setFlush_options(Boolean flush_options) {
        this.flush_options = flush_options;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /*  *//**
           * @return the categories
           */
    /*
     * public List<Category> getCategories() { return categories; }
     * 
     * 
     *//**
        * @param categories
        *            the categories to set
        *//*
           * public void setCategories(List<Category> categories) { this.categories = categories; }
           */

    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @return the platforms
     */
    public List<String> getPlatforms() {
        return platforms;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @param platforms
     *            the platforms to set
     */
    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

}
