/**
 * 
 */
package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * @author shameem
 *
 */
@Entity
public class Timings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String day;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=Slots.class)
    @JoinColumn(name = "storeTimeId", referencedColumnName="id")
    private List<Slots> timeSlots;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the day
     */
    public String getDay() {
        return day;
    }
    /**
     * @param day the day to set
     */
    public void setDay(String day) {
        this.day = day;
    }
    /**
     * @return the timeSlots
     */
    public List<Slots> getTimeSlots() {
        return timeSlots;
    }
    /**
     * @param timeSlots the timeSlots to set
     */
    public void setTimeSlots(List<Slots> timeSlots) {
        this.timeSlots = timeSlots;
    }
}
