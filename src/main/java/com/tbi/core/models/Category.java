/**
 * 
 */
package com.tbi.core.models;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;



/**
 * @author shameem
 *
 */
@Entity
public class Category implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Boolean active;
    private String description;
    private String img_url;
    private String name;
    private String ref_id;
    private Integer sort_order;
    private Integer uploadStatus;
    private String  parent_ref_id;
    private Integer storeId;
    
    @OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY)
    @JoinColumn(name="parent_ref_id",referencedColumnName="ref_id")
    private List<Category> category;
    
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Category.class)
    @JoinColumn(name = "categoryId", referencedColumnName = "id")
    private Set<Item> items;

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<Category> getCategory() {
		return category;
	}
	public void setCategory(List<Category> category) {
		this.category = category;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	/**
     * @return the sort_order
     */
    public Integer getSort_order() {
        return sort_order;
    }
    /**
     * @param sort_order the sort_order to set
     */
    public void setSort_order(Integer sort_order) {
        this.sort_order = sort_order;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @return the img_url
     */
    public String getImg_url() {
        return img_url;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param img_url the img_url to set
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }
    /**
     * @return the uploadStatus
     */
    public Integer getUploadStatus() {
        return uploadStatus;
    }
    /**
     * @param uploadStatus the uploadStatus to set
     */
    public void setUploadStatus(Integer uploadStatus) {
        this.uploadStatus = uploadStatus;
    }
    /**
     * @return the parent_ref_id
     */
    public String getParent_ref_id() {
        return parent_ref_id;
    }
    /**
     * @param parent_ref_id the parent_ref_id to set
     */
    public void setParent_ref_id(String parent_ref_id) {
        this.parent_ref_id = parent_ref_id;
    }
   
}
