/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class DeliveryPerson {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String alt_phone;
    private String name;
    private String phone;
    private String address;
    private String vehicleNo;
    private Boolean available;
    private Date joinDate;
    
    
    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }
    /**
     * @return the vehicleNo
     */
    public String getVehicleNo() {
        return vehicleNo;
    }
    /**
     * @return the available
     */
    public Boolean getAvailable() {
        return available;
    }
    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
    /**
     * @param vehicleNo the vehicleNo to set
     */
    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }
    /**
     * @param available the available to set
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the alt_phone
     */
    public String getAlt_phone() {
        return alt_phone;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param alt_phone the alt_phone to set
     */
    public void setAlt_phone(String alt_phone) {
        this.alt_phone = alt_phone;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * @return the joinDate
     */
    public Date getJoinDate() {
        return joinDate;
    }
    /**
     * @param joinDate the joinDate to set
     */
    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }
    
    
}
