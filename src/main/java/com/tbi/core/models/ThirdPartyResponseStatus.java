package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ThirdPartyResponseStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer updated;
    private Integer errors;
    private Integer created;
    private String  action;
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the updated
     */
    public Integer getUpdated() {
        return updated;
    }
    /**
     * @return the errors
     */
    public Integer getErrors() {
        return errors;
    }
    /**
     * @return the created
     */
    public Integer getCreated() {
        return created;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param updated the updated to set
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }
    /**
     * @param errors the errors to set
     */
    public void setErrors(Integer errors) {
        this.errors = errors;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Integer created) {
        this.created = created;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    
    

}
