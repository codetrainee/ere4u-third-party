/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class ItemActionSummary {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String item_refId;
    private String status;
    private String store_RefId;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the item_refId
     */
    public String getItem_refId() {
        return item_refId;
    }
   
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param item_refId the item_refId to set
     */
    public void setItem_refId(String item_refId) {
        this.item_refId = item_refId;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return the store_RefId
     */
    public String getStore_RefId() {
        return store_RefId;
    }
    /**
     * @param store_RefId the store_RefId to set
     */
    public void setStore_RefId(String store_RefId) {
        this.store_RefId = store_RefId;
    }
    

}
