/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class RiderDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String status;
    @CreatedDate
    private Date dateTime;
    private Integer refId;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = DeliveryPerson.class)
    @JoinColumn(name = "deliveryPersonId", referencedColumnName = "id")
    private DeliveryPerson deliveryPerson;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = DeliveryStatus.class)
    @JoinColumn(name="riderDetailId",referencedColumnName="id")
    private List<DeliveryStatus> deliveryStatus;
    
    
    /**
     * @return the refId
     */
    public Integer getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @return the deliveryPerson
     */
    public DeliveryPerson getDeliveryPerson() {
        return deliveryPerson;
    }
    /**
     * @return the deliveryStatus
     */
    public List<DeliveryStatus> getDeliveryStatus() {
        return deliveryStatus;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @param deliveryPerson the deliveryPerson to set
     */
    public void setDeliveryPerson(DeliveryPerson deliveryPerson) {
        this.deliveryPerson = deliveryPerson;
    }
    /**
     * @param deliveryStatus the deliveryStatus to set
     */
    public void setDeliveryStatus(List<DeliveryStatus> deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }
    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    
    
    
}
