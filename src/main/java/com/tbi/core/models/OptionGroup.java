/**
 * 
 */
package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * @author shameem
 *
 */
@Entity
public class OptionGroup {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer min_selectable;
    private Integer max_selectable;
    private Boolean active;
    private String refId;  
    private Integer quantity;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,targetEntity=Option.class,orphanRemoval=true)
    @JoinColumn(name = "optionGroupId", referencedColumnName="id")
    private List<Option> options;
    
    public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	/**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the min_selectable
     */
    public Integer getMin_selectable() {
        return min_selectable;
    }
    /**
     * @return the max_selectable
     */
    public Integer getMax_selectable() {
        return max_selectable;
    }
    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param min_selectable the min_selectable to set
     */
    public void setMin_selectable(Integer min_selectable) {
        this.min_selectable = min_selectable;
    }
    /**
     * @param max_selectable the max_selectable to set
     */
    public void setMax_selectable(Integer max_selectable) {
        this.max_selectable = max_selectable;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }
    /**
     * @return the options
     */
    public List<Option> getOptions() {
        return options;
    }
    /**
     * @param options the options to set
     */
    public void setOptions(List<Option> options) {
        this.options = options;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((max_selectable == null) ? 0 : max_selectable.hashCode());
        result = prime * result + ((min_selectable == null) ? 0 : min_selectable.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((options == null) ? 0 : options.hashCode());
        result = prime * result + ((refId == null) ? 0 : refId.hashCode());
        return result;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OptionGroup other = (OptionGroup) obj;
        if (active == null) {
            if (other.active != null)
                return false;
        } else if (!active.equals(other.active))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (max_selectable == null) {
            if (other.max_selectable != null)
                return false;
        } else if (!max_selectable.equals(other.max_selectable))
            return false;
        if (min_selectable == null) {
            if (other.min_selectable != null)
                return false;
        } else if (!min_selectable.equals(other.min_selectable))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (options == null) {
            if (other.options != null)
                return false;
        } else if (!options.equals(other.options))
            return false;
        if (refId == null) {
            if (other.refId != null)
                return false;
        } else if (!refId.equals(other.refId))
            return false;
        return true;
    }
    
    
          
}
