/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author shameem
 *
 */
@Entity
public class Extras {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Float cashCollected;
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the cashCollected
     */
    public Float getCashCollected() {
        return cashCollected;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param cashCollected the cashCollected to set
     */
    public void setCashCollected(Float cashCollected) {
        this.cashCollected = cashCollected;
    }
   
    
    
    
}
