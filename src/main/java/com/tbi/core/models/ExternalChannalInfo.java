/**
 * 
 */
package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;




/**
 * @author shameem
 *
 */
@Entity
public class ExternalChannalInfo{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private Integer s_id;
    private String deliveryType;
    private String name;
    private String kind;
    private String channalOrderId;
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = ExternalDiscount.class)
    @JoinColumn(name="channalId",referencedColumnName="id")
    private List<ExternalDiscount> externalDiscountInfo;
    
   
    
    
    /**
     * @return the s_id
     */
    public Integer getS_id() {
        return s_id;
    }
    /**
     * @return the channalOrderId
     */
    public String getChannalOrderId() {
        return channalOrderId;
    }
    /**
     * @param s_id the s_id to set
     */
    public void setS_id(Integer s_id) {
        this.s_id = s_id;
    }
    /**
     * @param channalOrderId the channalOrderId to set
     */
    public void setChannalOrderId(String channalOrderId) {
        this.channalOrderId = channalOrderId;
    }
    /**
     * @return the deliveryType
     */
    public String getDeliveryType() {
        return deliveryType;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @return the kind
     */
    public String getKind() {
        return kind;
    }
    /**
     * @return the externalDiscountInfo
     */
    public List<ExternalDiscount> getExternalDiscountInfo() {
        return externalDiscountInfo;
    }
    
    /**
     * @param deliveryType the deliveryType to set
     */
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @param kind the kind to set
     */
    public void setKind(String kind) {
        this.kind = kind;
    }
    /**
     * @param externalDiscountInfo the externalDiscountInfo to set
     */
    public void setExternalDiscountInfo(List<ExternalDiscount> externalDiscountInfo) {
        this.externalDiscountInfo = externalDiscountInfo;
    }
    
}
