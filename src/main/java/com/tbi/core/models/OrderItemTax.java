/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author shameem
 *
 */
@Entity
public class OrderItemTax {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Float rate;
    private String name;
    private Float value;
    
    
   
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the rate
     */
    public Float getRate() {
        return rate;
    }
    /**
     * @param rate the rate to set
     */
    public void setRate(Float rate) {
        this.rate = rate;
    }
    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }
    
    
    
}
