/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class OptionsToRemove {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
   
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
   
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
}
