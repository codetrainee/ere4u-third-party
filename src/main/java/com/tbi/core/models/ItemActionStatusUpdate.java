/**
 * 
 */
package com.tbi.core.models;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class ItemActionStatusUpdate {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String action;
    private String platform;
    @CreatedDate
    private Date date ;
    
    @OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL,targetEntity = ItemActionSummary.class)
    @JoinColumn(name ="ItemActionStatusId",referencedColumnName="id")
    private List<ItemActionSummary> itemActionSummaries;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @return the platform
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return the itemActionSummaries
     */
    public List<ItemActionSummary> getItemActionSummaries() {
        return itemActionSummaries;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @param platform the platform to set
     */
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @param itemActionSummaries the itemActionSummaries to set
     */
    public void setItemActionSummaries(List<ItemActionSummary> itemActionSummaries) {
        this.itemActionSummaries = itemActionSummaries;
    }
    
    
    

}
