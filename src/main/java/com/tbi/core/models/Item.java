/**
 * 
 */
package com.tbi.core.models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author shameem
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private Boolean available;
	private Integer currentStock;
	private String description;
	private String foodType;
	private Integer price;
	private String refId;
	private String img_url;
	private Boolean sold_at_store;
	private Boolean recommended;
	private String action;
	private Integer uploadStatus;
	private String img_name;
	private String shortDescription;
	private String fullDescription;
	private double discountPrice;
	private double discountPercentage;
	private String categoryId;
	private Integer quantity;
	private Integer storeId;
	
	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Integer getQuantity() {
		return quantity;
	} 

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getFullDescription() {
		return fullDescription;
	}

	public void setFullDescription(String fullDescription) {
		this.fullDescription = fullDescription;
	}

	public double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}

	
	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}


	public String getImg_name() {
		return img_name;
	}

	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}


	@ElementCollection
	private List<String> platforms;

	@OneToOne(targetEntity = Tags.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private Tags tags;

    /*
     * @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
     * 
     * @JoinColumn(name = "itemId", referencedColumnName = "id") private Set<Category> categories;
     */

	@Transient
	private Set<Category> categories;
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Charges.class)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private List<Charges> charges;

	@OneToMany(cascade = CascadeType.MERGE, targetEntity = ItemImages.class)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private List<ItemImages> itemImages;
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Tax.class)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private List<Tax> taxes;

	@OneToMany(cascade = CascadeType.ALL, targetEntity = OptionGroup.class, orphanRemoval = true)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private List<OptionGroup> optionGroup;

	
	public List<ItemImages> getItemImages() {
		return itemImages;
	}

	public void setItemImages(List<ItemImages> itemImages) {
		this.itemImages = itemImages;
	}

	/**
	 * @return the tags
	 */
	public Tags getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(Tags tags) {
		this.tags = tags;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the platforms
	 */
	public List<String> getPlatforms() {
		return platforms;
	}

	/**
	 * @param platforms
	 *            the platforms to set
	 */
	public void setPlatforms(List<String> platforms) {
		this.platforms = platforms;
	}

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }     

    /**
	 * @return the sold_at_store
	 */
	public Boolean getSold_at_store() {
		return sold_at_store;
	}

	/**
	 * @return the recommended
	 */
	public Boolean getRecommended() {
		return recommended;
	}

	/**
	 * @param sold_at_store
	 *            the sold_at_store to set
	 */
	public void setSold_at_store(Boolean sold_at_store) {
		this.sold_at_store = sold_at_store;
	}

	/**
	 * @param recommended
	 *            the recommended to set
	 */
	public void setRecommended(Boolean recommended) {
		this.recommended = recommended;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the available
	 */
	public Boolean getAvailable() {
		return available;
	}

	/**
	 * @return the currentStock
	 */
	public Integer getCurrentStock() {
		return currentStock;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the foodType
	 */
	public String getFoodType() {
		return foodType;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param available
	 *            the available to set
	 */
	public void setAvailable(Boolean available) {
		this.available = available;
	}

	/**
	 * @param currentStock
	 *            the currentStock to set
	 */
	public void setCurrentStock(Integer currentStock) {
		this.currentStock = currentStock;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param foodType
	 *            the foodType to set
	 */
	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}

	/**
	 * @param refId
	 *            the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}

	/**
	 * @return the img_url
	 */
	public String getImg_url() {
		return img_url;
	}

	/**
	 * @param img_url
	 *            the img_url to set
	 */
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	/**
	 * @return the charges
	 */
	public List<Charges> getCharges() {
		return charges;
	}

	/**
	 * @return the taxes
	 */
	public List<Tax> getTaxes() {
		return taxes;
	}

	/**
	 * @return the optionGroup
	 */
	public List<OptionGroup> getOptionGroup() {
		return optionGroup;
	}

	/**
	 * @param charges
	 *            the charges to set
	 */
	public void setCharges(List<Charges> charges) {
		this.charges = charges;
	}

	/**
	 * @param taxes
	 *            the taxes to set
	 */
	public void setTaxes(List<Tax> taxes) {
		this.taxes = taxes;
	}

	/**
	 * @param optionGroup
	 *            the optionGroup to set
	 */
	public void setOptionGroup(List<OptionGroup> optionGroup) {
		this.optionGroup = optionGroup;
	}

	/**
	 * @return the uploadStatus
	 */
	public Integer getUploadStatus() {
		return uploadStatus;
	}

	/**
	 * @param uploadStatus
	 *            the uploadStatus to set
	 */
	public void setUploadStatus(Integer uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((available == null) ? 0 : available.hashCode());
//		result = prime * result + ((categories == null) ? 0 : categories.hashCode());
		result = prime * result + ((charges == null) ? 0 : charges.hashCode());
		result = prime * result + ((currentStock == null) ? 0 : currentStock.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((foodType == null) ? 0 : foodType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((img_url == null) ? 0 : img_url.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((optionGroup == null) ? 0 : optionGroup.hashCode());
		result = prime * result + ((platforms == null) ? 0 : platforms.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((recommended == null) ? 0 : recommended.hashCode());
		result = prime * result + ((refId == null) ? 0 : refId.hashCode());
		result = prime * result + ((sold_at_store == null) ? 0 : sold_at_store.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((taxes == null) ? 0 : taxes.hashCode());
		result = prime * result + ((uploadStatus == null) ? 0 : uploadStatus.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
        /*
         * if (categories == null) { if (other.categories != null) return false; } else if (!categories.equals(other.categories)) return false;
         */
		if (charges == null) {
			if (other.charges != null)
				return false;
		} else if (!charges.equals(other.charges))
			return false;
		if (currentStock == null) {
			if (other.currentStock != null)
				return false;
		} else if (!currentStock.equals(other.currentStock))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (foodType == null) {
			if (other.foodType != null)
				return false;
		} else if (!foodType.equals(other.foodType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (img_url == null) {
			if (other.img_url != null)
				return false;
		} else if (!img_url.equals(other.img_url))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (optionGroup == null) {
			if (other.optionGroup != null)
				return false;
		} else if (!optionGroup.equals(other.optionGroup))
			return false;
		if (platforms == null) {
			if (other.platforms != null)
				return false;
		} else if (!platforms.equals(other.platforms))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (recommended == null) {
			if (other.recommended != null)
				return false;
		} else if (!recommended.equals(other.recommended))
			return false;
		if (refId == null) {
			if (other.refId != null)
				return false;
		} else if (!refId.equals(other.refId))
			return false;
		if (sold_at_store == null) {
			if (other.sold_at_store != null)
				return false;
		} else if (!sold_at_store.equals(other.sold_at_store))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (taxes == null) {
			if (other.taxes != null)
				return false;
		} else if (!taxes.equals(other.taxes))
			return false;
		if (uploadStatus == null) {
			if (other.uploadStatus != null)
				return false;
		} else if (!uploadStatus.equals(other.uploadStatus))
			return false;
		return true;
	}

}
