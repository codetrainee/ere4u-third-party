/**
 * 
 */
package com.tbi.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */

@Entity
public class ExternalDiscount {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Boolean is_merchant_discount;
    private Float rate;
    private String name;
    private Float value;
    private String code;
  
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the is_merchant_discount
     */
    public Boolean getIs_merchant_discount() {
        return is_merchant_discount;
    }
    /**
     * @return the rate
     */
    public Float getRate() {
        return rate;
    }
  
    /**
     * @return the value
     */
    public Float getValue() {
        return value;
    }
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param is_merchant_discount the is_merchant_discount to set
     */
    public void setIs_merchant_discount(Boolean is_merchant_discount) {
        this.is_merchant_discount = is_merchant_discount;
    }
    /**
     * @param rate the rate to set
     */
    public void setRate(Float rate) {
        this.rate = rate;
    }
  
    /**
     * @param value the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }
    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
  
    
}
