/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * @author shameem
 *
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class DeliveryStatus {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String comments;
    @CreatedDate
    private Date created;
    private String status;
   
    
    
   
    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }
    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }
   
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }
   
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
