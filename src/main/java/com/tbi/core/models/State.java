package com.tbi.core.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class State {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String stateName;
	private String stateCode;
	@OneToMany(cascade = CascadeType.ALL,fetch= FetchType.LAZY,targetEntity =City.class)
    @JoinColumn(name="stateId",referencedColumnName="id")
    private List<City> citys;
	
	
	public Integer getId() { 
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public List<City> getCitys() {
		return citys;
	} 
	public void setCitys(List<City> citys) {
		this.citys = citys;
	}

	
}
