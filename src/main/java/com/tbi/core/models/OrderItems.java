/**
 * 
 */
package com.tbi.core.models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author shameem
 *
 */
@Entity
public class OrderItems {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private Float discount;
    private String foodType;
    private String image_landscape_url;
    private String image_url;
    private Float price;
    private Integer quantity;
    private String name;
    private Float total;
    private Float total_with_tax;
    private Integer unit_weight;
    private String refId;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = OrderItemTax.class)
    @JoinColumn(name="orderItemId",referencedColumnName="id")
    private List<OrderItemTax> taxes;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = OptionsToAdd.class)
    @JoinColumn(name="orderItemId",referencedColumnName="id")
    private List<OptionsToAdd> options_to_add;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = OptionsToRemove.class)
    @JoinColumn(name="orderItemId",referencedColumnName="id")
    private List<OptionsToRemove> options_to_remove;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = Charges.class)
    @JoinColumn(name="orderItemId",referencedColumnName="id")
    private List<Charges> charges;
    
    @ManyToMany(targetEntity = Item.class)
	@JoinColumn(name = "itemId", referencedColumnName = "id")
	private List<Item> Item;
    
    
    public List<Item> getItem() {
		return Item;
	}
	public void setItem(List<Item> item) {
		Item = item;
	}
   public String getRefId() {
        return refId;
    }
    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
   
    /**
     * @return the charges
     */
    public List<Charges> getCharges() {
        return charges;
    }
    /**
     * @param charges the charges to set
     */
    public void setCharges(List<Charges> charges) {
        this.charges = charges;
    }
    /**
     * @return the discount
     */
    public Float getDiscount() {
        return discount;
    }
    /**
     * @param discount the discount to set
     */
    public void setDiscount(Float discount) {
        this.discount = discount;
    }
    /**
     * @return the foodType
     */
    public String getFoodType() {
        return foodType;
    }
    /**
     * @param foodType the foodType to set
     */
    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }
    /**
     * @return the image_landscape_url
     */
    public String getImage_landscape_url() {
        return image_landscape_url;
    }
    /**
     * @param image_landscape_url the image_landscape_url to set
     */
    public void setImage_landscape_url(String image_landscape_url) {
        this.image_landscape_url = image_landscape_url;
    }
    /**
     * @return the image_url
     */
    public String getImage_url() {
        return image_url;
    }
    /**
     * @param image_url the image_url to set
     */
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
    /**
     * @return the options_to_add
     */
    public List<OptionsToAdd> getOptions_to_add() {
        return options_to_add;
    }
    /**
     * @param options_to_add the options_to_add to set
     */
    public void setOptions_to_add(List<OptionsToAdd> options_to_add) {
        this.options_to_add = options_to_add;
    }
    /**
     * @return the options_to_remove
     */
    public List<OptionsToRemove> getOptions_to_remove() {
        return options_to_remove;
    }
    /**
     * @param options_to_remove the options_to_remove to set
     */
    public void setOptions_to_remove(List<OptionsToRemove> options_to_remove) {
        this.options_to_remove = options_to_remove;
    }
    /**
     * @return the price
     */
    public Float getPrice() {
        return price;
    }
    /**
     * @param price the price to set
     */
    public void setPrice(Float price) {
        this.price = price;
    }
    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }
    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    /**
     * @return the taxes
     */
    public List<OrderItemTax> getTaxes() {
        return taxes;
    }
    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(List<OrderItemTax> taxes) {
        this.taxes = taxes;
    }
    /**
     * @return the total
     */
    public Float getTotal() {
        return total;
    }
    /**
     * @param total the total to set
     */
    public void setTotal(Float total) {
        this.total = total;
    }
    /**
     * @return the total_with_tax
     */
    public Float getTotal_with_tax() {
        return total_with_tax;
    }
    /**
     * @param total_with_tax the total_with_tax to set
     */
    public void setTotal_with_tax(Float total_with_tax) {
        this.total_with_tax = total_with_tax;
    }
    /**
     * @return the unit_weight
     */
    public Integer getUnit_weight() {
        return unit_weight;
    }
    /**
     * @param unit_weight the unit_weight to set
     */
    public void setUnit_weight(Integer unit_weight) {
        this.unit_weight = unit_weight;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    
    
    
}
