/**
 * 
 */
package com.tbi.core.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


/**
 * @author shameem
 *
 */
@Entity
public class OrderDetails {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String coupon;
    private Date created;
    private Date deliveryDateTime;
    private Float discount;
    private String instructions;
    private Float totalChargesAmount;
    private Float totalTaxesAmount;
    private Float subtotal;
    private Float total;
    private String type;
    private String state;
    private Float totalCharges;
    private Float totalExternalDiscount;
    private Float totalTaxes;
    private Float itemTotalCharges;
    private Float itemTotalTaxes;
    private Float itemTaxes;
    private String merchant_ref_id; //ClientStoreRefId
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = Tax.class)
    @JoinColumn(name="orderDetailsId",referencedColumnName="id")
    private List<Tax> taxes;
    
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, targetEntity = Charges.class)
    @JoinColumn(name="orderDetailsId",referencedColumnName="id")
    private List<Charges> charges;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the coupon
     */
    public String getCoupon() {
        return coupon;
    }

   
    

    /**
     * @return the discount
     */
    public Float getDiscount() {
        return discount;
    }

    /**
     * @return the instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     * @return the totalChargesAmount
     */
    public Float getTotalChargesAmount() {
        return totalChargesAmount;
    }

    /**
     * @return the totalTaxesAmount
     */
    public Float getTotalTaxesAmount() {
        return totalTaxesAmount;
    }

    /**
     * @return the subtotal
     */
    public Float getSubtotal() {
        return subtotal;
    }

    /**
     * @return the total
     */
    public Float getTotal() {
        return total;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @return the totalCharges
     */
    public Float getTotalCharges() {
        return totalCharges;
    }

    /**
     * @return the totalExternalDiscount
     */
    public Float getTotalExternalDiscount() {
        return totalExternalDiscount;
    }

    /**
     * @return the totalTaxes
     */
    public Float getTotalTaxes() {
        return totalTaxes;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param coupon the coupon to set
     */
    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

  

  
    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the deliveryDateTime
     */
    public Date getDeliveryDateTime() {
        return deliveryDateTime;
    }

    /**
     * @param deliveryDateTime the deliveryDateTime to set
     */
    public void setDeliveryDateTime(Date deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    /**
     * @param instructions the instructions to set
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     * @param totalChargesAmount the totalChargesAmount to set
     */
    public void setTotalChargesAmount(Float totalChargesAmount) {
        this.totalChargesAmount = totalChargesAmount;
    }

    /**
     * @param totalTaxesAmount the totalTaxesAmount to set
     */
    public void setTotalTaxesAmount(Float totalTaxesAmount) {
        this.totalTaxesAmount = totalTaxesAmount;
    }

    /**
     * @param subtotal the subtotal to set
     */
    public void setSubtotal(Float subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Float total) {
        this.total = total;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @param totalCharges the totalCharges to set
     */
    public void setTotalCharges(Float totalCharges) {
        this.totalCharges = totalCharges;
    }

    /**
     * @param totalExternalDiscount the totalExternalDiscount to set
     */
    public void setTotalExternalDiscount(Float totalExternalDiscount) {
        this.totalExternalDiscount = totalExternalDiscount;
    }

    /**
     * @param totalTaxes the totalTaxes to set
     */
    public void setTotalTaxes(Float totalTaxes) {
        this.totalTaxes = totalTaxes;
    }


    /**
     * @return the itemTotalCharges
     */
    public Float getItemTotalCharges() {
        return itemTotalCharges;
    }

    /**
     * @return the itemTotalTaxes
     */
    public Float getItemTotalTaxes() {
        return itemTotalTaxes;
    }

    /**
     * @return the itemTaxes
     */
    public Float getItemTaxes() {
        return itemTaxes;
    }

    /**
     * @param itemTotalCharges the itemTotalCharges to set
     */
    public void setItemTotalCharges(Float itemTotalCharges) {
        this.itemTotalCharges = itemTotalCharges;
    }

    /**
     * @param itemTotalTaxes the itemTotalTaxes to set
     */
    public void setItemTotalTaxes(Float itemTotalTaxes) {
        this.itemTotalTaxes = itemTotalTaxes;
    }

    /**
     * @param itemTaxes the itemTaxes to set
     */
    public void setItemTaxes(Float itemTaxes) {
        this.itemTaxes = itemTaxes;
    }

    /**
     * @return the charges
     */
    public List<Charges> getCharges() {
        return charges;
    }

    /**
     * @param charges the charges to set
     */
    public void setCharges(List<Charges> charges) {
        this.charges = charges;
    }

    /**
     * @return the taxes
     */
    public List<Tax> getTaxes() {
        return taxes;
    }

    /**
     * @param taxes the taxes to set
     */
    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }

    /**
     * @return the merchant_ref_id
     */
    public String getMerchant_ref_id() {
        return merchant_ref_id;
    }

    /**
     * @param merchant_ref_id the merchant_ref_id to set
     */
    public void setMerchant_ref_id(String merchant_ref_id) {
        this.merchant_ref_id = merchant_ref_id;
    }
    
}
