/**
 * 
 */
package com.tbi.core.models;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author shameem
 *
 */
@Entity
public class Tags {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @ElementCollection
    private List<String> swiggy;
    
    @ElementCollection
    private List<String> zomato;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the swiggy
     */
    public List<String> getSwiggy() {
        return swiggy;
    }

    /**
     * @return the zomato
     */
    public List<String> getZomato() {
        return zomato;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param swiggy the swiggy to set
     */
    public void setSwiggy(List<String> swiggy) {
        this.swiggy = swiggy;
    }

    /**
     * @param zomato the zomato to set
     */
    public void setZomato(List<String> zomato) {
        this.zomato = zomato;
    }
    
    

}
