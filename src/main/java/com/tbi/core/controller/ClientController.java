/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Client;
import com.tbi.core.service.ClientService;

/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientService customerService;

    @GetMapping()
    public List<Client> getAllClients() {
        return customerService.findAll();
    }

    @PostMapping()
    public Client save(@Valid @RequestBody Client customer) throws Exception {
        return customerService.save(customer);
    }

    @GetMapping("/{id}")
    public Client getClientById(@PathVariable(value = "id") Integer CustomerId) {
        return customerService.findById(CustomerId);
    }

    @PutMapping("/{id}")
    public String updateClient(@PathVariable(value = "id") Integer CustomerId, @Valid @RequestBody Client customerDetails) throws Exception {
        Client customer = customerService.findById(CustomerId);
        ModelMapper mapper = new ModelMapper();
        customer = mapper.map(customerDetails, Client.class);
        customer.setId(CustomerId);
        customerService.save(customer);
        return "200 : Update Success";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Client> deleteClient(@PathVariable(value = "id") Integer CustomerId) {
        Client Customer = customerService.findById(CustomerId);
        customerService.delete(Customer);
        return ResponseEntity.ok().build();
    }

}
