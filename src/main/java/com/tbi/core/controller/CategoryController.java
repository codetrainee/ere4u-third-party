package com.tbi.core.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Category;
import com.tbi.core.service.CategoryService;
@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/{id}")
	public List<Category> findByStoreId(@PathVariable(value = "id") Integer storeId) {
		return categoryService.findByStoreId(storeId);
	}
	
	
}
