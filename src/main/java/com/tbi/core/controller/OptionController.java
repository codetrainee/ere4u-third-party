package com.tbi.core.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tbi.core.models.Option;
import com.tbi.core.service.OptionService;

@CrossOrigin
@RestController
@RequestMapping("/option")
public class OptionController {
	@Autowired
	private OptionService optionService;

	@PostMapping("/save")
	public Option saveOption(@RequestBody Option option) throws Exception {
		return optionService.saveOption(option);
	}
	
	@GetMapping("/fetch")
	public List<Option> fetchAllOption(){
		return optionService.fetchAllOption();
	}
	@PutMapping("/update")
    public String updateOption(@Valid @RequestBody Option  option) throws Exception {
		optionService.updateOption(option);
        return  "200 : Update Success";
   }
	@GetMapping("/fetch/{id}")
	public Option findById(@PathVariable Integer id) {
		return optionService.findById(id);
	}
}
