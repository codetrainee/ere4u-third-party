/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.tbi.core.models.Category;
import com.tbi.core.models.FileInfo;
import com.tbi.core.models.Item;
import com.tbi.core.models.ItemImages;
import com.tbi.core.repository.ItemImagesRepository;
import com.tbi.core.service.CategoryService;
import com.tbi.core.service.ItemService;

/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	ItemService itemService;
	@Autowired
    private Environment env;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ItemImagesRepository itemImagesRepository;

	@GetMapping("/all")
	public List<Item> getAllItem() {
		List<Item> item= itemService.findAll();
		 for(Item category : item) {
		if(category.getCategoryId() !=null){
			Set<Category> cat=categoryService.findById(Integer.valueOf(category.getCategoryId()));
			category.setCategories(cat);
		}
		 }
		return item;
	}

	

	@PostMapping("/saveItem")
	public Item createItem(@Valid @RequestBody Item Item) throws Exception {
		return itemService.save(Item);
	}

	@GetMapping("/{id}")
	public Item getItemById(@PathVariable(value = "id") Integer ItemId) {
		Item item=itemService.findById(ItemId);
		if(item.getCategoryId() !=null){
			Set<Category> cat=categoryService.findById(Integer.valueOf(item.getCategoryId()));
			item.setCategories(cat);;
		}
		return item;
	}

	@PutMapping("/update")
	    public String updateItem(@Valid @RequestBody Item  item) throws Exception {
	         itemService.updateItem(item);
	        return  "200 : Update Success";
	   }
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Item> deleteItem(@PathVariable(value = "id") Integer itemId) {
		if (itemId != null) {
			itemService.delete(itemId);
		}
		return ResponseEntity.ok().build();
	}

	/* fetch image */
	@CrossOrigin
	@GetMapping("/files")
	public ResponseEntity<List<FileInfo>> getListFiles() {

		List<FileInfo> fileInfos = itemService.loadAll().map(path -> {

			String filename = path.getFileName().toString();
			String url = MvcUriComponentsBuilder
					.fromMethodName(ItemController.class, "getFile", path.getFileName().toString()).build().toString();

			return new FileInfo(filename, url);
		}).collect(Collectors.toList());

		return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	}
	// upload image
	@CrossOrigin
	@PostMapping("/{id}")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file,
			@PathVariable(value = "id") Integer id) {
		String message = "";
		String loc1 = env.getProperty("Upload.folder.location");
		try {
			ItemImages itemImages = new ItemImages();
			itemService.saveImg(file);
			Item items = itemService.findById(id);
			itemImages.setImg_name(file.getOriginalFilename());
			itemImages.setImg_url(loc1);
			itemImages.setItemId(items.getId()); 
			itemImagesRepository.save(itemImages);
			message = "Uploaded the file successfully: " + file.getOriginalFilename();
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
		} catch (Exception e) {
			message = "Could not upload the file: " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
		}
	}
	@CrossOrigin
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = itemService.load(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	@CrossOrigin
	/* fetch image */
	@GetMapping("/file/{id}")
	public FileInfo loadFileById(@PathVariable(value = "id") Integer id) {

		
		Item items = itemService.findById(id);

		String filename = items.getImg_name().toString();
		String url = MvcUriComponentsBuilder.fromMethodName(ItemController.class, "getFile", items.getImg_name().toString())
				.build().toString();

		return new FileInfo(filename, url);

		

	}

}
