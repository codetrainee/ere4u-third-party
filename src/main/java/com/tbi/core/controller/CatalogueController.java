/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Catalogue;
import com.tbi.core.service.CatalogueService;

import okhttp3.Response;

/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/catalogue")
public class CatalogueController {

    @Autowired
    private CatalogueService catalogueService;

    @PostMapping("/{storeId}")
    public Response save(@RequestBody Catalogue catalogue, @PathVariable String storeId) throws Exception {
        return catalogueService.save(catalogue, storeId);
    }

    @GetMapping()
    public List<Catalogue> getAll() throws Exception {
        return catalogueService.getAll();
    }
    
    @GetMapping("/{id}")
    public Catalogue findById(@PathVariable Integer id) throws Exception {
        return catalogueService.findOne(id);
    }

    /**
     * @author shameem
     * at a time disable/disableAll items and enable/enableAll items 
     * 
     */
    @PostMapping("/itemAction/{storeId}")
    public Response updateItemAction(@RequestBody Catalogue catalogue, @PathVariable Integer storeId) throws Exception {
        return catalogueService.updateItemAction(catalogue, storeId);
    }

    @PutMapping()
    public Catalogue updateCatalogue(@RequestBody Catalogue catalogue) throws Exception {
        return catalogueService.update(catalogue);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteCatalogue(@PathVariable(value = "id") Integer catalogueId) {
        catalogueService.delete(catalogueId);
        return ResponseEntity.ok().build();
    }

}
