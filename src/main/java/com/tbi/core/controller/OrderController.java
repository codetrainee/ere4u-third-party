package com.tbi.core.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Order;
import com.tbi.core.models.OrderState;
import com.tbi.core.models.Params;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.models.User;
import com.tbi.core.repository.OrderRepsitory;
import com.tbi.core.service.OrderService;
import com.tbi.core.service.ParamService;
import com.tbi.core.service.UserService;

import okhttp3.Response;


/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderRepsitory repo;
    @Autowired
    OrderService orderService; 
    @Autowired
    UserService userService;
	@PostMapping("/save")
	public  Order save(@RequestBody Order order) throws Exception {

		order.setCurrentDate(new Date());
		order.setRefId(orderService.OrderIdGenerator());
	   return orderService.saveOrder(order);
	}
	
	@GetMapping()
	public List<Order> get() throws Exception {
	    return orderService.getAll();
	}
	
	@GetMapping("/get/{storeId}/{currentState}")
    public List<Order> getOrdersByStoreId(@PathVariable Integer storeId,@PathVariable String currentState){
        return orderService.findOrdersByStoreIdAndcurrentState(storeId,currentState.replace("+", " "));
    }
	
	@GetMapping("/{storeId}")
    public List<Order> getThirdPartyOrdersByStoreId(@PathVariable Integer storeId ,@QueryParam(value = "currentState") String currentState){
	    return orderService.getThirdPartyOrdersByStoreIdAndStatus(storeId,currentState);
    }
    
	@GetMapping("status/{id}")
    public OrderState getByStatus(@PathVariable(value ="id") Integer orderId) throws Exception {
	    return orderService.findByCurrentState(orderId);
    }
	
	@PutMapping("status/{id}/{storeId}")
	public  Response orderStatusUpdate(@PathVariable(value ="id") Integer orderId,@PathVariable(value ="storeId") String storeId,@RequestBody OrderState orderStatus) throws Exception {
        return orderService.updateOrderStatus(orderId,orderStatus,storeId);
	}
	
	@PutMapping()
    public  Order updateOrder(@RequestBody Order order) throws Exception {
       return orderService.updateOrder(order);
    }
	
	@DeleteMapping("/{id}")
    public  ResponseEntity<Order> deleteOrder(@PathVariable(value = "id") Integer orderId) {
         orderService.delete(orderId);
        return ResponseEntity.ok().build();
    }
	
	@GetMapping("/getOne/{id}")
	public Order findById(@PathVariable Integer id) {
		return orderService.findById(id);
	}
	@GetMapping("/data/{storeId}/{userId}")
	 public List<Order> findByStoreIdAndUserId(@PathVariable("storeId") Integer storeId,@PathVariable("userId") Long userId){ 
		List<Order> orders = orderService.findByStoreIdAndUserId(storeId,userId);
		if(!orders.isEmpty()){
		User user = userService.findByUserId1(userId);
		orders.get(0).setUser(user);
		}
		return orders;
		}
	
}
