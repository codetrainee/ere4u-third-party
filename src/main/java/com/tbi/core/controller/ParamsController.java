package com.tbi.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Item;
import com.tbi.core.models.Params;
import com.tbi.core.models.User;
import com.tbi.core.service.ParamService;

@RestController
@RequestMapping("/param")
public class ParamsController {
	@Autowired
	private ParamService paramService;
	
	@PostMapping("/save")
	public Params saveParams(@Valid @RequestBody Params desc){
		return paramService.save(desc);
	}
	@GetMapping("/all")
	public List<Params> getAllParams(){
		return paramService.findAll();
		
	}
	@GetMapping("/{id}")
	public Params getParamsById(@PathVariable(value = "id") Integer id){
		return paramService.findById(id);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Params> deleteParams(@PathVariable(value = "id") Integer id) {
		if (id != null) {
			paramService.delete(id);
		}
		return ResponseEntity.ok().build();
	}
	
	@PutMapping("/update")
	public Params updateParams(@RequestBody Params desc) throws Exception {
		return paramService.updateParamsDescription(desc);
	}
	
	
	
	
	

}
