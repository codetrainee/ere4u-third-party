package com.tbi.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Pincode;
import com.tbi.core.models.Store;
import com.tbi.core.service.PincodeService;
import com.tbi.core.service.StoreService;

@CrossOrigin
@RestController
@RequestMapping("/pincode")
public class PincodeController {
	@Autowired
	private PincodeService pincodeService;
	
	
	@GetMapping("/fetchAll")
	public List<Pincode> fetchAllPincode(){
		return pincodeService.fetchAllPincode();
	}
	@PostMapping("/save")
	public Pincode savePincode(@RequestBody Pincode pincode)throws Exception {
		return pincodeService.savePincode(pincode);
	}
	@GetMapping("/{id}")
	public Pincode findById(@PathVariable Integer id) {
		return pincodeService.findById(id);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Pincode> deletePincode(@PathVariable(value = "id") Integer id) {
		if (id != null) {
			pincodeService.deletePincode(id);
		}
		return ResponseEntity.ok().build();
	}
	@PutMapping("/update")
    public String updatePincode(@Valid @RequestBody Pincode  pincode) throws Exception {
		pincodeService.updatePincode(pincode);
        return  "200 : Update Success";
   }
	
	@GetMapping("/code/{code}/{store}")
	public Pincode findPincodeByCodeAndStore(@PathVariable(value = "code") Long code,@PathVariable(value = "store") Integer store) {
		Pincode pin = pincodeService.findPincodeByCodeAndStore(String.valueOf(code),String.valueOf(store));
		return pin;		
		}
		
		

}
