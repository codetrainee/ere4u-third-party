package com.tbi.core.controller;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.models.User;
import com.tbi.core.service.OTPDetailService;
import com.tbi.core.service.ParamService;
import com.tbi.core.service.UserService;
import com.tbi.core.serviceImpl.OTPDetailServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/otp")
public class OTPDetailsController {
	
	@Autowired
	private OTPDetailServiceImpl service;
	@Autowired
	private OTPDetailService ser;
	@Autowired
	private ParamService paramService;
	@Autowired
	private UserService userService;
	@CrossOrigin
	@PostMapping("/generateOtp")
	public OTPDetail saveOTPDetail(@RequestBody OTPDetail otp) {
		OTPDetail noExists = service.findByTypeValue(otp.getTypeValue());
		
		try {
			if (noExists != null) {
				int genOtp = service.generateOTP(otp.getTypeValue());
				otp.setId(noExists.getId());
				otp.setTypeValue(otp.getTypeValue());
				otp.setOtp(genOtp);
			    otp.setStatus("pending");
			    otp.setType("MOB");
			    otp.setCurrentDate(new Date()); 
			    otp.setOtpValidateDate(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));
				 ser.updateOTPDetail(otp);
				 service.checkStatusForSendSms(genOtp,otp.getTypeValue());
			} else {
				int genOtp = service.generateOTP(otp.getTypeValue());
				otp.setTypeValue(otp.getTypeValue());
				otp.setOtp(genOtp);
			    otp.setStatus("pending");
			    otp.setType("MOB");
			    otp.setCurrentDate(new Date()); 
			    otp.setOtpValidateDate(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));
				OTPDetail id = ser.save(otp);
				service.checkStatusForSendSms(genOtp,otp.getTypeValue());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return otp;
	}
	@CrossOrigin
	@PutMapping("/mobile/authenticate")
	public ResponseEntity<String> updateOtpDetail(@RequestBody OTPDetail otpd) {
		ResponseEntity<String> resp = null;
		OTPDetail last = service.findByTypeValue(otpd.getTypeValue());
		Date nowDate =new Date();
		try {		
			if (last.getTypeValue().equals(otpd.getTypeValue())){
				if(last.getOtp().equals(otpd.getOtp())){
				if(nowDate.compareTo(last.getOtpValidateDate()) < 0){
					last.setStatus("verified");			
				ser.save(last);
			    resp = new ResponseEntity<String>(ResponseCode.OTP_VERIFIED_OK_CODE,HttpStatus.OK);
				}
				}else{
					resp = new ResponseEntity<String>(ResponseCode.OTP_NOT_EXIST_CODE,HttpStatus.OK);
				}
				}else{
					resp = new ResponseEntity<String>(ResponseCode.OTP_EXPIRED_CODE,HttpStatus.OK);
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}	
		return resp;	
	}
	
	@PostMapping("/forgatePassword")
	public ResponseEntity<User> saveProduct(@RequestBody User userreg,OTPDetail otp) {
		ResponseEntity<User> resp = null;
		User userExists = userService.findByMobileNo(userreg.getMobileNo());
		otp.setTypeValue(Long.toString(userreg.getMobileNo()));
		try {
			if (userExists != null) {
				int genOtp = service.generateOTP(otp.getTypeValue());
				//otp.setTypeValue(Long.toString(userreg.getMobileNo()));
				otp.setTypeValue(otp.getTypeValue());
				otp.setOtp(genOtp);
			    otp.setStatus("pending");
			    otp.setType("MOB");
			    otp.setCurrentDate(new Date()); 
			    otp.setOtpValidateDate(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));
			    OTPDetail noExists = service.findByTypeValue(otp.getTypeValue());
			    if (noExists != null) {
			    	otp.setId(noExists.getId());
			    	 ser.updateOTPDetail(otp);
			    	 service.checkStatusForSendSms(genOtp,otp.getTypeValue());
			    }else{
			    	
			    	 OTPDetail id = ser.save(otp);	
			    	 service.checkStatusForSendSms(genOtp,otp.getTypeValue());
			    }
			    userExists.setStatus("OTP "+genOtp);
				resp = new ResponseEntity<User>(userExists,
						HttpStatus.ACCEPTED);
			} else {
				 
				resp = new ResponseEntity<User>(userExists,HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			resp = new ResponseEntity<User>(userExists, HttpStatus.INTERNAL_SERVER_ERROR // Our App Got Exception
			);
			e.printStackTrace();
		}
		return resp;
	}
	@GetMapping("/{typeValue}")
	public ResponseEntity<String> findByTypeValue(@PathVariable(value = "typeValue") String typeValue) {
		ResponseEntity<String> resp = null;
		OTPDetail noExists = service.findByTypeValue(typeValue);
		if(noExists != null) {
			resp = new ResponseEntity<String>(ResponseCode.ALREADY_REGISTERED,HttpStatus.OK);
			System.out.println("ok");
		}else {
			resp = new ResponseEntity<String>(ResponseCode.NOT_REGISTERED,HttpStatus.BAD_REQUEST);
			System.out.println("not ok");
		}
		return resp;
	}
	
}
