package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Item;
import com.tbi.core.models.Option;
import com.tbi.core.models.OptionGroup;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.service.ItemService;
import com.tbi.core.service.OptionGroupService;
import com.tbi.core.service.OptionService;

@CrossOrigin
@RestController
@RequestMapping("/optionGroup")
public class OptionGroupController {
	@Autowired
	private OptionGroupService optionGroupService;
	@Autowired
	private OptionService optionService;
	@Autowired
	private ItemService itemService;
	@GetMapping("/fetch")
	public List<OptionGroup> fetchAllOptionGroup(){
		return optionGroupService.fetchAllOptionGroup();
	}
	
	@GetMapping("/fetch/{id}")
	public List<OptionGroup> findById(@PathVariable Integer id) {
		return optionGroupService.findById(id);
		
		
	}
	/*@GetMapping("/{refId}")
	public Item fetchItem(@PathVariable List<String> refId) {
		List<OptionGroup> idExists = optionGroupService.findByRefId(refId);
		//List<Item> i = null;
		Integer Id=idExists.get(0).getItemId();
		//if (idExists != null) {
			//i= itemService.findByItemId(idExists.getItemId());
		//}
		return id;
		}
	*/
}
