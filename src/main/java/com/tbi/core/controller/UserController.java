package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tbi.core.models.AddressUser;
import com.tbi.core.models.City;
import com.tbi.core.models.Order;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.models.State;
import com.tbi.core.models.User;
import com.tbi.core.repository.CityRepository;
import com.tbi.core.service.StateWiseCityService;
import com.tbi.core.service.UserService;

/* Author Shrikant Mishra
	Date ==>7/07/2020
*/
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService service;
	@Autowired
	private CityRepository cityrepository;
	@Autowired
	private StateWiseCityService stateWiseCityService;
	
	@PostMapping("/save")
	public User save(@RequestBody User user) throws Exception {
		return service.saveUser(user);

	}

	/*
	 * Author Shrikant Mishra date : 01/04/2021
	 */
	@GetMapping("/all")
	public List<User> get() throws Exception {
		List<User> user=service.getAll();
		/*
		 * This method fetch all city of particular state which is clear because not
		 * required to show now remove this code if necessary as per requirement.
		 */
		for(User userObject:user) {
			userObject.getState().getCitys().clear();
		}
		return user;
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Order> deleteUser(@PathVariable(value = "id") Long userId) {
		service.delete(userId);
		return ResponseEntity.ok().build();
	}
	@CrossOrigin
	@PutMapping("/update")
	public User updateUser(@RequestBody User user) throws Exception {
		AddressUser au=new AddressUser(); 
		if(user.getUserId()!=null){
			User userExists = service.findByUserId1(user.getUserId());
			user.setPassword(userExists.getPassword());	
		}
		return service.updateUser(user);
	}

	@PostMapping("/uploadImage")
	public ResponseEntity<String> uploadImage(@RequestParam("imageFile") MultipartFile imageFile) {
		ResponseEntity<String> resp = null;
		try {

			service.saveImage(imageFile);
			resp= new ResponseEntity<String>("File Uploaded successfully", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return resp;
	}

	@PostMapping("/saveReg")
	public ResponseEntity<String> saveProduct(@RequestBody User userreg) {
		ResponseEntity<String> resp = null;
		User userExists = service.findByMobileNo(userreg.getMobileNo());

		try {
			
			if (userExists != null) {
				resp = new ResponseEntity<String>("Given MobileNo '" + userreg.getMobileNo() + "' Data already exist",
						HttpStatus.BAD_REQUEST);

			} else {
				User id = service.saveUser(userreg);
				resp = new ResponseEntity<String>("Registration '" + userreg.getMobileNo() + "' created Successfully!",
						HttpStatus.OK 
				
				);
			}

		} catch (Exception e) {
			resp = new ResponseEntity<String>("Unable to Save Registration", HttpStatus.INTERNAL_SERVER_ERROR // Our App Got Exception
			);
			e.printStackTrace();
		}
		return resp;
	}
	
	@PostMapping("/verify/user")
	public ResponseEntity<String> verifyUser(@RequestBody User user) {
		ResponseEntity<String> resp = null;
		User userExists = service.findByUsername(user.getUsername());

		try {
			if (userExists != null) {
				resp = new ResponseEntity<String>("User Exists code "+ResponseCode.USER_EXISTS,HttpStatus.OK);
			} else { 
				resp = new ResponseEntity<String>("User Name Not Found "+ResponseCode.NOT_USER_EXISTS ,HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			resp = new ResponseEntity<String>("Unable to Get User Name", HttpStatus.INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return resp;
	}
	/*
	 * Author Shrikant Mishra date : 01/04/2021
	 */
	@GetMapping("/{userName}")
	public User getUserByUserName(@PathVariable(value = "userName") String username) {
		User user = service.findByUsername(username);
		/*
		 * This method fetch all city of particular state which is clear because not
		 * required to show now remove this code if necessary as per requirement.
		 */
		user.getState().getCitys().clear();
		return user;
	}
	
	@PutMapping("/changePassword/{oldPassword}")
	public ResponseEntity<String> changePassword(@RequestBody User user, @PathVariable ("oldPassword")String oldPassword) throws Exception {
		ResponseEntity<String> resp = null;
		User userExists = service.findByUserId1(user.getUserId());
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String userPass=userExists.getPassword();	
		boolean isPasswordMatch = passwordEncoder.matches(oldPassword, userPass);
		if (isPasswordMatch == true) {
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			userExists.setPassword(encodedPassword);
			service.updateUser(userExists);
			resp = new ResponseEntity<String>("Password Change",HttpStatus.ACCEPTED );
		} else { 
			resp = new ResponseEntity<String>("Old password not match",HttpStatus.BAD_REQUEST );
		}
		
		return resp;
	}

}
