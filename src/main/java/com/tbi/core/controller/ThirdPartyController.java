/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.ThirdParty;
import com.tbi.core.repository.ThirdPartyRepository;


/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/third-party")
public class ThirdPartyController {
	
	@Autowired
	ThirdPartyRepository thirdPartyRepository;
	
	@GetMapping()
	public  List<ThirdParty> getAllThirdPartys() {
		return thirdPartyRepository.findAll();
	}

	@PostMapping()
	public  ThirdParty createThirdParty(@Valid @RequestBody ThirdParty ThirdParty) {
		return thirdPartyRepository.save(ThirdParty);
	}

	@GetMapping("/{id}")
	public ThirdParty getThirdPartyById(@PathVariable(value = "id") Integer ThirdPartyId) {
		return thirdPartyRepository.findById(ThirdPartyId);
	}

	@PutMapping()
	public  ThirdParty updateThirdParty(@RequestBody ThirdParty ThirdPartyDetails) throws Exception {
		return  thirdPartyRepository.save(ThirdPartyDetails);
	}

	@DeleteMapping("/{id}")
	public  ResponseEntity<Object> deleteThirdParty(@PathVariable(value = "id") Integer ThirdPartyId) {
		ThirdParty ThirdParty = thirdPartyRepository.findById(ThirdPartyId);
		thirdPartyRepository.delete(ThirdParty);
		return ResponseEntity.ok().build();
	}

}
