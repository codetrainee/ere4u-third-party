/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.DeliveryPerson;
import com.tbi.core.models.RiderDetails;
import com.tbi.core.service.RiderService;


/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/rider")
public class RiderController {

    @Autowired
    RiderService riderService;
    
    @GetMapping("/order/{orderId}")
    public RiderDetails findRiderByOrderId(@PathVariable(value = "orderId") Integer orderId) {
        return riderService.findRiderByOrderId(orderId);
    }
    
    @PutMapping("/status/{orderId}")
    public void updateRiderStatus(@PathVariable Integer orderId, @RequestBody RiderDetails riderDetails) {
       riderService.updateRiderStatus(riderDetails,orderId);
    }
    
    @GetMapping()
    public List<DeliveryPerson> getAll() {
        return riderService.findAll();
    }

    @PostMapping()
    public DeliveryPerson save(@Valid @RequestBody DeliveryPerson deliveryPerson) throws Exception {
         return riderService.save(deliveryPerson);
    }

    @GetMapping("/{id}")
    public DeliveryPerson getRiderById(@PathVariable(value = "id") Integer riderId) {
        return riderService.findById(riderId);
    }

    @PutMapping()
    public  String update(@Valid @RequestBody DeliveryPerson  deliveryPerson) throws Exception {
         riderService.update(deliveryPerson);
        return  "200 : Update Success";
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity<Object> delete(@PathVariable(value = "id") Integer riderId) {
        if(riderId != null) {
            riderService.delete(riderId);
        }
        return ResponseEntity.ok().build();
    }
    
}
