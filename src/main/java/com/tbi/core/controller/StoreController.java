/**
 * 
 */
package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Client;
import com.tbi.core.models.Store;
import com.tbi.core.service.StoreService;

import okhttp3.Response;


/**
 * @author shameem
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/store")
public class StoreController {
    
    @Autowired
    private StoreService storeService;
    
	@PutMapping("/action")
	public Response storeAction(@RequestBody Store store) throws Exception {
	    return storeService.updateStoreAction(store);
	}
	   
    @PostMapping("/{clientId}")
    public Client save(@PathVariable Integer clientId,@RequestBody Store Store) throws Exception {
       return storeService.save(Store,clientId);
    }
    
    @GetMapping("/{id}")
    public Store findStoreById(@PathVariable Integer id) throws Exception {
        return storeService.findStoreById(id);
    }
    
    @GetMapping("findByRefId/{refId}")
    public Store findStoreByRefId(@PathVariable String refId) throws Exception {
        return storeService.findStoreByRefId(refId);
    }
    
    
    @GetMapping()
    public List<Store> getAll() throws Exception {
        return storeService.getAll();
    } 
    
    @PutMapping()
    public Store updateStore(@RequestBody Store Store) throws Exception {
       return storeService.updateStore(Store);
    }
    
    @DeleteMapping("/{id}")
    public void deleteStore(@PathVariable Integer id) {
         storeService.delete(id);
    }

}
