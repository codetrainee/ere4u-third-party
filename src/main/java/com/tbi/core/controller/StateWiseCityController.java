package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.City;
import com.tbi.core.models.State;
import com.tbi.core.repository.CityRepository;
import com.tbi.core.service.StateWiseCityService;

@RestController
@CrossOrigin
@RequestMapping("/stateWiseCity")
public class StateWiseCityController { 
	@Autowired
	private StateWiseCityService stateWiseCityService;
	@Autowired
	private CityRepository cityrepository;
	@GetMapping("/all")
	public List<State> get() throws Exception {
		return stateWiseCityService.findAll();
	}
	@GetMapping("/{id}")
	public State getStateById(@PathVariable(value = "id") Integer id) {
		return stateWiseCityService.findById(id);
	}
	/*
	 * Author Shrikant Mishra date : 01/04/2021
	 */
	@GetMapping("city/{id}")
	public City getCityById(@PathVariable(value = "id") Integer id) {
		return cityrepository.findOne(id);
	}
}
 