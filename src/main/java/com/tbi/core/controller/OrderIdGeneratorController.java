package com.tbi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tbi.core.models.Params;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.service.ParamService;
@CrossOrigin
@RestController
@RequestMapping("/orderIdGenerator")
public class OrderIdGeneratorController {
	
	@Autowired
	private ParamService paramService;
	@PostMapping("/generateOrderId") 
	public void OrderIdGenerator()  {
		List<Params> paramList = paramService.findByTypeAndClientId(ResponseCode.GENERATE_ORDER_ID, 1);
		int id=0;
		for (Params p : paramList) { 
			if (p.getKey().equals("GENERATE_ORDER_ID")) {
				 id = p.getRefId();
				
			}
			for(int i = id; i<= 1000000; i++)
	        {
	            System.out.println(i);
	        }
		}
		
		
		
	}
	

}
