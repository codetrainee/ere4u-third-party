/**
 * 
 */
package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.Catalogue;
import com.tbi.core.models.Store;

import okhttp3.Response;

/**
 * @author shameem
 *
 */
public interface CatalogueService {

    List<Catalogue> getAll();

    Catalogue update(Catalogue catalogue);

    void delete(Integer catalogueId);

    Response save(Catalogue catalogue, String storeId) throws Exception;

    Response updateItemAction(Catalogue catalogue, Integer storeId) throws Exception;

    Catalogue findOne(Integer catalogueId);
}
