package com.tbi.core.service;

import com.tbi.core.models.OTPDetail;

public interface OTPDetailService {
	public OTPDetail save(OTPDetail otp);
	public OTPDetail findByTypeValue(String typeValue);
	public OTPDetail updateOTPDetail(OTPDetail otp);
	public OTPDetail findByOtp(Integer opt);
	
	
	

}
