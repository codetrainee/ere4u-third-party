package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.Catalogue;
import com.tbi.core.models.Order;
import com.tbi.core.models.Store;

import okhttp3.Response;

public interface ProviderService {

    public Response updateOrderStatus(Order order, String authKey) throws Exception;

    public void updateRiderStatus(Order order, String authKey);

    public Response saveStore(List<Store> store, String authKey) throws Exception;

    public Response updateStoreAction(Store store, String authKey) throws Exception;

    public Response saveCatalogue(Store store, String authKey) throws Exception;

    public Response updateItemAction(Catalogue catalogue, String storeId, String authKey) throws Exception;
}
