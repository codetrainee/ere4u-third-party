/**
 * 
 */
package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.DeliveryPerson;
import com.tbi.core.models.RiderDetails;

/**
 * @author shameem
 *
 */
public interface RiderService {

    List<DeliveryPerson> findAll();

    DeliveryPerson save(DeliveryPerson deliveryPerson);

    DeliveryPerson findById(Integer riderId);

    void update(DeliveryPerson deliveryPerson);

    void delete(Integer itemId);

    void updateRiderStatus(RiderDetails riderDetails, Integer orderId);

    RiderDetails findRiderByOrderId(Integer orderId);
}
