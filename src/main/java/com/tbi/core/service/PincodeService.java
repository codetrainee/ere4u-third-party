package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.Pincode;
import com.tbi.core.models.Store;

public interface PincodeService {
	
	public List<Pincode> fetchAllPincode();
	
	public Pincode savePincode(Pincode pincode);
	
	public Pincode findById(Integer id);
	
	public void deletePincode(Integer id);
	
	public void updatePincode(Pincode pincode);
	 public Pincode findPincodeByCodeAndStore(String code,String store); 

}
