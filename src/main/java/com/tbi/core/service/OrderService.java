/**
 * 
 */
package com.tbi.core.service;



import java.util.List;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.Order;
import com.tbi.core.models.OrderState;
import okhttp3.Response;

/**
 * @author shameem
 *
 */
public interface OrderService {

    Order saveOrder(Order order);

    List<Order> getAll();
 
    Order updateOrder(Order order);

    void delete(Integer orderId);

    Response updateOrderStatus(Integer orderId,OrderState orderStatus, String storeId) throws Exception;

    OrderState findByCurrentState(Integer orderId);

    List<Order> findOrdersByStoreIdAndcurrentState(Integer storeId, String currentState);

    Order findOrderByRefId(Integer refId);

    List<Order> getThirdPartyOrdersByStoreIdAndStatus(Integer storeId, String currentState);
    
    public Order findById(Integer id);
    
    List<Order> findByStoreIdAndUserId(Integer storeId,Long userId);
    
    public Order findTopByOrderByRefIdDesc();

	public Integer OrderIdGenerator();
    
}
