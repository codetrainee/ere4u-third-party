package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.Params;
import com.tbi.core.models.User;


public interface ParamService {
	
	List<Params> findAll();

	public Params save(Params desc);

	public Params findById(Integer id);

	public void delete(Integer id);

	public Params updateParamsDescription(Params desc);
	
	public List<Params> findByTypeAndClientId(String type,Integer clientId);
	
	void save(List<Params> param);
	
	/*public OTPDetail findByTypeValue(Integer otp);*/
}
