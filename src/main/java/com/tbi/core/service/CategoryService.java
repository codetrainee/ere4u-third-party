package com.tbi.core.service;

import java.util.List;
import java.util.Set;

import com.tbi.core.models.Category;

public interface CategoryService {
	public List<Category> findByStoreId(Integer storeId);
	public Category findCategoryByName(String name);
	public Category saveCategory(Category category);
	public Category updateCategory(Category category);
	public Set<Category> findById(Integer id);
	List<Category> findCategoryByNameAndStoreId(String name, Integer storeId);


}
 