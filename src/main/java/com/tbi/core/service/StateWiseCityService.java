package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.State;

public interface StateWiseCityService {
	public List<State> findAll();
	
	public State findById(Integer id);
	
	
}
  