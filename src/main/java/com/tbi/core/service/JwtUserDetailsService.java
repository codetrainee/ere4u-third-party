package com.tbi.core.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.tbi.core.models.UseDto;
import com.tbi.core.models.User;
import com.tbi.core.repository.UserRepository;
import com.tbi.core.serviceImpl.UserServiceImpl;



@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;//userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Autowired
	private UserServiceImpl userService;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	
	public User save(UseDto user) {
		User newUser = new User();
		if(user.getUserId()!=null){
			User userExists = userService.findByUserId1(user.getUserId());
			userExists.setPassword(bcryptEncoder.encode(user.getPassword()));
			return userService.updateUser(userExists);
		}
		else{
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userRepository.save(newUser);
		}
	}
}