/**
 * 
 */
package com.tbi.core.service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.tbi.core.models.Item;

/**
 * @author shameem
 *
 */

public interface ItemService {

	List<Item> findAll();

	Item save(Item item);

	Item findById(Integer itemId);

	void delete(Integer itemId);
	
	 void update(Item item);

	public Item updateItem(Item id);

	public Stream<Path> loadAll();

	public void saveImg(MultipartFile file) throws IOException;

	 void init() throws IOException; 

	Resource load(String filename);

	void deleteAll();
	
	public List<Item> findById(List<Integer> id);

}
