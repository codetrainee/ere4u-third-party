package com.tbi.core.service;

import java.util.List;
import com.tbi.core.models.Option;

public interface OptionService {
	
	public Option saveOption(Option option);
	
	public List<Option> fetchAllOption();
	
	public void updateOption(Option option);
	
	public Option findById(Integer id);
}
