package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.OptionGroup;

public interface OptionGroupService {
	
	public List<OptionGroup> fetchAllOptionGroup();
	public List<OptionGroup> findById(Integer id);

}
