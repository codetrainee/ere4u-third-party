/**
 * 
 */
package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.Client;

/**
 * @author shameem
 *
 */
public interface ClientService {

	List<Client> findAll();

	Client save(Client customer) throws Exception;

	Client findById(Integer customerId);

	void delete(Client customer);

}
