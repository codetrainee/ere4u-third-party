/**
 * 
 */
package com.tbi.core.service;

import java.util.List;

import com.tbi.core.models.Client;
import com.tbi.core.models.Store;

import okhttp3.Response;


/**
 * @author shameem
 *
 */
public interface StoreService {
    
    Response updateStoreAction(Store store) throws Exception;

    Client save(Store store, Integer clientId);

    List<Store> getAll();

    Store updateStore(Store store);

    void delete(Integer storeId);

    Store findStoreById(Integer id);

    Store findStoreByRefId(String refId);
}
