package com.tbi.core.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.tbi.core.models.User;

/* Author Shrikant Mishra
	Date ==>7/07/2020
*/
public interface UserService {

	public User saveRegister(User userreg);

	public User saveUser(User user);

	public List<User> getAll();

	public void delete(Long userId);

	public User updateUser(User user);

	public boolean isUserExist(Long mobileNo);

	public User findByMobileNo(Long mobileNo);

	public User uploadImages(User image);

	public Optional<User> findByUserId(Long userId);

	public void saveImage(MultipartFile imageFile) throws Exception;

	public User findByUsername(String userName);
	
	public User findByUserId1(Long userId);
	
	

}
