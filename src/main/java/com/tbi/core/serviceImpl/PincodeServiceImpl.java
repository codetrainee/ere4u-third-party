package com.tbi.core.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Pincode;
import com.tbi.core.models.Store;
import com.tbi.core.repository.PincodeRepository;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.service.PincodeService;

@Service
public class PincodeServiceImpl implements PincodeService {
	@Autowired
	private PincodeRepository pincodeRepository;
	@Autowired
	private StoreRepository storeRepository;

	@Override
	public List<Pincode> fetchAllPincode() {
		return pincodeRepository.findAll();
	}

	@Override
	public Pincode savePincode(Pincode pincode) {
		if (pincode.getStore().getId() != null) {
			pincode.setStore(storeRepository.findStoreByRefId(String.valueOf(pincode.getStore().getId())));
		}
		return pincodeRepository.save(pincode);
	}

	@Override
	public Pincode findById(Integer id) {
		return pincodeRepository.findOne(id);
	}

	@Override
	public void deletePincode(Integer id) {
		pincodeRepository.delete(id);
	}

	@Override
	public void updatePincode(Pincode pincode) {
		pincodeRepository.save(pincode);
	}

	
	@Override
	public Pincode findPincodeByCodeAndStore(String code,String store) {
		Store stores = storeRepository.findStoreByRefId(store);
		if(stores != null) {
		return pincodeRepository.findPincodeByCodeAndStore(code,stores);
		}else {
			return new Pincode();
		}
	}



}
