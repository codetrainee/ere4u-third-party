package com.tbi.core.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.OptionGroup;
import com.tbi.core.repository.OptionGroupRepository;
import com.tbi.core.service.OptionGroupService;
import com.tbi.core.service.OptionService;
@Service
public class OptionGroupServiceImpl implements OptionGroupService {
@Autowired
private OptionGroupRepository optionGroupRepository;



@Override
public List<OptionGroup> fetchAllOptionGroup() {
	return optionGroupRepository.findAll(); 
}



@Override
public List<OptionGroup> findById(Integer id) {
	return optionGroupRepository.findById(id);
}


}
