/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.tbi.core.models.Item;
import com.tbi.core.repository.ItemRepository;
import com.tbi.core.service.ItemService;

/**
 * @author shameem
 * @param <PhotoDto>
 *
 */
@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	final static Path root = Paths.get("/home/faisal/apache-tomcat-7.0.94/webapps/uploads/");
	
	 @Override 
	  public void init() throws IOException {
		 if (Files.exists(root)) {
			}else{
				 Files.createDirectory(root);
			}
	 }
	@Autowired
	ItemRepository itemRepository;

	@Override
	public List<Item> findAll() {
		return itemRepository.findAll();
	}

	@Override
	public Item save(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public Item findById(Integer itemId) {
		return itemRepository.findOne(itemId);
	}
	 @Override
	   public void update(Item item) {
	         itemRepository.save(item);
	     }

	@Override
	public void delete(Integer itemId) {
		Item item = itemRepository.findOne(itemId);
		if (item != null) {
			itemRepository.delete(item);
		}
	}

	
	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
		} catch (IOException e) {
			throw new RuntimeException("Could not load the files!");
		}
	}

	@Override
	public void saveImg(MultipartFile file) throws IOException {
		// Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

		Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()),  StandardCopyOption.REPLACE_EXISTING);
	}

	@Override
	public Resource load(String filename) {
		try {
			Path file = root.resolve(filename);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("Could not read the file!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}

	@Override
	@Transactional
	///@Modifying(flushAutomatically = true, clearAutomatically = true)
	public Item updateItem(Item itemObj) {
		// Item itm = itemRepository.findOne(itemObj.getId());

		return itemRepository.save(itemObj);
	}

	@Override
	public void deleteAll() {
		 FileSystemUtils.deleteRecursively(root.toFile());
		
	}

	@Override
	public List<Item> findById(List<Integer> id) {
		return itemRepository.findById(id);
	}

	
	
	

}
