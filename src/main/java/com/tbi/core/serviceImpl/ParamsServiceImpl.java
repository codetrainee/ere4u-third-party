package com.tbi.core.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.Params;
import com.tbi.core.repository.ParamRepository;
import com.tbi.core.service.ParamService;
@Service
public class ParamsServiceImpl implements ParamService {
	
	@Autowired
	private ParamRepository repository;
	
	@Override
	public List<Params> findAll() {
		return repository.findAll();
	}

	@Override
	public Params save(Params desc) {
		return repository.save(desc);
	}

	@Override
	public Params findById(Integer id) {
		return repository.findOne(id);
	}

	@Override
	public void delete(Integer id) {
		Params paramdesc=repository.findOne(id);
		if(paramdesc != null){
			repository.delete(id);
		}
	}

	@Override
	public Params updateParamsDescription(Params desc) {
		return repository.save(desc);
	}

	@Override
	public List<Params> findByTypeAndClientId(String type,Integer clientId){
	return repository.findByTypeAndClientId(type,clientId);
	}

	@Override
	public void save(List<Params> param) {
		repository.save(param)	;	
	}

	/*@Override
	public OTPDetail findByTypeValue(Integer otp) {
		return repository.findByTypeValue(otp);
	}
*/
	
}
