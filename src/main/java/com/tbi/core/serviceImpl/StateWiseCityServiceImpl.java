package com.tbi.core.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.State;
import com.tbi.core.repository.StateWiseCityRepository;
import com.tbi.core.service.StateWiseCityService;

@Service
public class StateWiseCityServiceImpl implements StateWiseCityService {
	@Autowired
	private StateWiseCityRepository stateWiseCityRepository;
	

	@Override
	public List<State> findAll() { 
		return stateWiseCityRepository.findAll();
	}

	@Override 
	public State findById(Integer id) {
		return stateWiseCityRepository.findOne(id);
	}

	
}
