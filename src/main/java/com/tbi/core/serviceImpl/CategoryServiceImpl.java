package com.tbi.core.serviceImpl;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Category;
import com.tbi.core.repository.CategoryRepository;
import com.tbi.core.service.CategoryService;
@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryRepository catRepository;

	@Override
	public List<Category> findByStoreId(Integer storeId) {
		return catRepository.findByStoreId(storeId);
	}
	
	@Override 
	public Category findCategoryByName(String name) {
	    return catRepository.findCategoryByName(name);
	}
	
	@Override
	public Category saveCategory(Category category) {
	    return catRepository.save(category);
	}
	
	@Override
	public Category updateCategory(Category category) {
        return catRepository.save(category);
    }

	@Override
	public Set<Category> findById(Integer id) {
		return catRepository.findById(id);
	}
	
	@Override
	public List<Category> findCategoryByNameAndStoreId(String name, Integer storeId) {
		return catRepository.findCategoryByNameAndStoreId(name, storeId);
	}
}
