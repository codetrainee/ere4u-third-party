/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Catalogue;
import com.tbi.core.models.Category;
import com.tbi.core.models.Item;
import com.tbi.core.models.Store;
import com.tbi.core.repository.CatalogueRepository;
import com.tbi.core.repository.CategoryRepository;
import com.tbi.core.repository.ItemRepository;
import com.tbi.core.repository.OptionGroupRepository;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.repository.TaxRepository;
import com.tbi.core.service.CatalogueService;
import com.tbi.core.service.CategoryService;
import com.tbi.core.service.ItemService;
import com.tbi.core.service.ProviderService;

import okhttp3.Response;

/**
 * @author shameem
 *
 */
@Service
public class CatalogueServiceImpl implements CatalogueService {

    @Autowired
    StoreRepository storeRepository;
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    CatalogueRepository catalogueRepository;
    @Autowired
    public ItemRepository itemRepository;
    @Autowired
    OptionGroupRepository optionGroupRepository;
    @Autowired
    CategoryService categoryService;    
    @Autowired
    TaxRepository taxRepository;    
    @Autowired
    ItemService itemService;

    private static Logger logger = Logger.getLogger(CatalogueServiceImpl.class);

    @Override
    public List<Catalogue> getAll() {
        List<Catalogue> catalogues = catalogueRepository.findAll();
        return catalogues;
    }

    @Override
    public void delete(Integer catalogueId) {
        Catalogue catalogue = catalogueRepository.findOne(catalogueId);
        catalogueRepository.delete(catalogue);
    }

    @Override
    public Catalogue update(Catalogue catalogue) {
        return catalogueRepository.save(catalogue);
    }

    @Override
    public Response save(Catalogue newCatalogue, String storeId) throws Exception {
        ModelMapper mapper = new ModelMapper();
        Store store = storeRepository.findStoreByRefId(storeId);
        if (store != null) {
            if(newCatalogue.getFlush_items()) {
                store.setCatalogue(null);
                store = storeRepository.save(store);
            }
            List<Item> items = new ArrayList<>();
            Catalogue existCatalogue = store.getCatalogue();
            if (existCatalogue != null) {
                for (Item newItem : newCatalogue.getItems()) {
                    List<Item> existItem = itemRepository.findItemByRefIdAndAndStoreId(newItem.getRefId(), newItem.getStoreId());
                    
                    if (existItem != null && !existItem.isEmpty()) {
                    	
                        items.add(updateExistItem(existItem.get(0), newItem, mapper));
                    } else {
                        newCatalogue.setId(existCatalogue.getId());
                        newCatalogue
                                .setItems(Stream.of(existCatalogue.getItems(), newCatalogue.getItems()).flatMap(x -> x.stream()).collect(Collectors.toList()));
                        store.setCatalogue(newCatalogue);
                    }
                }
                if (!items.isEmpty()) {
                    List<Item> existItems = existCatalogue.getItems();
                    existCatalogue.setItems(items);
                    store.setCatalogue(existCatalogue);
                }
            } else {
                store.setCatalogue(newCatalogue);
            }
            try {
            store = storeRepository.save(store);
            }catch (Exception e) {
                e.printStackTrace();
            }
            this.insertCategory(newCatalogue.getItems(), store);
            
            store = UpdateCatalogueUploadStatus(store);
            return saveCatalogue(store);
        } else {
            throw new RuntimeException("Store details not found on refId : " + storeId);
        }
    }

    private void insertCategory(List<Item> items, Store store) {
		for (Item newItem : items) {
			List<Item> existItem = itemRepository.findItemByRefIdAndAndStoreId(newItem.getRefId(), Integer.parseInt(store.getRefId()));
			for (Category category : newItem.getCategories()) {
				List<Category> category2 = categoryService.findCategoryByNameAndStoreId(category.getName(),
						Integer.parseInt(store.getRefId()));
				Category category3 = null;
				if (category2 != null && !category2.isEmpty()) {
					category3 = category2.get(0);
					category.setId(category3.getId());
					category.setStoreId(Integer.parseInt(store.getRefId()));
					categoryService.updateCategory(category);
				} else {
					category.setStoreId(Integer.parseInt(store.getRefId()));
					category.setId(0);
					category3 = categoryService.saveCategory(category);
				}
				if (!existItem.isEmpty()) {
					existItem.get(0).setCategoryId(String.valueOf(category3.getId()));
				} else {
					newItem.setCategoryId(String.valueOf(category3.getId()));
				}
			}
			if (!existItem.isEmpty()) {
				existItem.get(0).setStoreId(Integer.parseInt(store.getRefId()));
				itemService.update(existItem.get(0));
			} else {
				newItem.setStoreId(Integer.parseInt(store.getRefId()));
				itemService.save(newItem);
			}
		}
			
    }
    
    private Item updateExistItem(Item existItem, Item newItem, ModelMapper mapper) {
        if(!existItem.equals(newItem)) {
            newItem.setUploadStatus(0);
            newItem.setId(existItem.getId());
            existItem = mapper.map(newItem, Item.class);  
        }
        return existItem;
    }

    private Store UpdateCatalogueUploadStatus(Store store) {
        try {
            List<Item> items = store.getCatalogue().getItems();
            items.removeIf(item -> item.getUploadStatus().equals(1));
            store.getCatalogue().setItems(new ArrayList<Item>());
            store.getCatalogue().setItems(items);
            for (Item item : items) {
                item.setUploadStatus(1);
                itemRepository.setUploadStatusByuploadStatusAndid(item.getUploadStatus(), item.getId());
            }
        } catch (Exception e) {
            logger.error("Catalogue update upload status faild !");
        }
        return store;
    }

    private Response saveCatalogue(Store store) throws Exception {
        ProviderService providerService = (ProviderService) applicationContext.getBean(store.getProvider());
        return providerService.saveCatalogue(store,store.getAuthKey());
    }

    @Override
    public Response updateItemAction(Catalogue catalogue, Integer storeId) throws Exception {
        Store store = storeRepository.findStoreByRefId(String.valueOf(storeId));
        List<Item> items = new ArrayList<>();
        if (store != null) {
            store.getCatalogue().getItems().forEach(eItem -> {
                catalogue.getItems().forEach(newItem -> {
                    if(eItem.getRefId().equals(newItem.getRefId())) {
                        eItem.setAction(catalogue.getAction());
                        items.add(eItem);
                    }
                });
            });
        }
        store = storeRepository.save(store);
        catalogue.setItems(items);
        return updateItemAction(catalogue,store);
    }

    private Response updateItemAction(Catalogue catalogue,Store store) throws Exception {
        ProviderService providerService = (ProviderService) applicationContext.getBean(store.getProvider());
        return providerService.updateItemAction(catalogue,store.getRefId(),store.getAuthKey());
    }

    @Override
    public Catalogue findOne(Integer catalogueId) {
        Catalogue catalogue = catalogueRepository.findOne(catalogueId);
        return catalogue;
    }

}
