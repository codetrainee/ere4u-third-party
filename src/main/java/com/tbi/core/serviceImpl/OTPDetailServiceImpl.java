package com.tbi.core.serviceImpl;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tbi.core.models.OTPDetail;
import com.tbi.core.models.Params;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.repository.OTPDetailsRepository;
import com.tbi.core.service.OTPDetailService;
import com.tbi.core.service.ParamService;

@Service
public class OTPDetailServiceImpl implements OTPDetailService {

	@Autowired
	private ParamService paramService;
	@Autowired
	private OTPDetailsRepository repo;

	public int generateOTP(String key) {
		Random random = new Random();
		int otp = 1000 + random.nextInt(9000);
		return otp;
	}

	@Override
	public OTPDetail save(OTPDetail otp) {
		return repo.save(otp);
	}

	@Override
	public OTPDetail findByTypeValue(String typeValue) {
		return repo.findByTypeValue(typeValue);
	}

	@Override
	public OTPDetail updateOTPDetail(OTPDetail otp) {
		return repo.save(otp);
	}

	@Override
	public OTPDetail findByOtp(Integer opt) {
		return repo.findByOtp(opt);
	}

	public void sendSMS(Integer genOtp) {
		OTPDetail od = findByOtp(genOtp);
		String urlT = "", userName = "", pass = "", route = "", sender = "", number = "", sms = "";
		StringBuilder url = new StringBuilder();
		StringBuilder msg = new StringBuilder();
		RestTemplate restTemplate = new RestTemplate();
		List<Params> paramList = paramService.findByTypeAndClientId(ResponseCode.SMS_DETAIL, 1);
		System.out.println(paramList);
		for (Params p : paramList) {
			if (p.getKey().equals("OTP_URL")) {
				urlT = p.getValue();
			}
			if (p.getKey().equals("OTP_USERNAME")) {
				userName = p.getValue();
			}
			if (p.getKey().equals("OTP_PASSWORD")) {
				pass = p.getValue();
			}
			if (p.getKey().equals("OTP_ROUTE")) {
				route = p.getValue();
			}
			if (p.getKey().equals("OTP_SENDERID")) {
				sender = p.getValue();
			}
		}
		if (od.getTypeValue().equals(od.getTypeValue())) {
			number = od.getTypeValue();
		}
		if (genOtp.equals(od.getOtp())) {
			sms = genOtp +" "+ ResponseCode.OTP_MSG;
		}
		url.append(urlT);
		url.append("uname=");
		url.append(userName);
		url.append("&pwd=");
		url.append(pass);
		url.append("&route=");
		url.append(route);
		url.append("&senderid=");
		url.append(sender);
		url.append("&to=");
		url.append(number);
		url.append("&msg=");
		url.append(sms);

		url.append(msg);

		restTemplate.getForObject(url.toString(), String.class);
	}

	public void checkStatusForSendSms(int genOtp, String typeValue) {

		try {
			OTPDetail last = findByTypeValue(typeValue);;
			if (last.getStatus().equals(ResponseCode.STATUS_PENDING)) {
				sendSMS(genOtp);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
