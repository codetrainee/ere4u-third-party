/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Client;
import com.tbi.core.models.Store;
import com.tbi.core.repository.ClientRepository;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.service.ProviderService;
import com.tbi.core.service.StoreService;

import okhttp3.Response;

/**
 * @author shameem
 *
 */
@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    StoreRepository storeRepository;
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    ClientRepository clientRepository;

    public Response saveStore(Client client) throws Exception {
        List<Store> stores = storeRepository.save(client.getStores());
        List<Store> storeList = new ArrayList<>();
        for(Store store :stores) {
            ProviderService providerService = (ProviderService) applicationContext.getBean(store.getProvider());
            storeList.add(store);
            return providerService.saveStore(storeList, store.getAuthKey());
        }
        return null;
    }

    @Override
    public Response updateStoreAction(Store store) throws Exception {
        Store store2 = storeRepository.findStoreByRefId(store.getRefId());
        store2.setAction(store.getAction());
        store2.setPlatforms(store.getPlatforms());
        store2 = storeRepository.save(store2);
        ProviderService providerService = (ProviderService) applicationContext.getBean(store2.getProvider());
        return providerService.updateStoreAction(store2, store2.getAuthKey());
    }

    @Override
    public Client save(Store store, Integer clientId) {
        List<Store> stores = new ArrayList<>();
        Client client = new Client();
        client = clientRepository.findOne(clientId);
        client.setId(clientId);
        stores = client.getStores();
        stores.add(store);
        client.setStores(stores);
        return clientRepository.save(client);
    }

    @Override
    public List<Store> getAll() {
        return storeRepository.findAll();
    }

    @Override
    public Store updateStore(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public void delete(Integer storeId) {
        Store store = storeRepository.findOne(storeId);
        storeRepository.delete(store);
    }

    @Override
    public Store findStoreById(Integer storeId) {
        return storeRepository.findOne(storeId);
    }

    @Override
    public Store findStoreByRefId(String refId) {
        return storeRepository.findStoreByRefId(refId);
    }
}
