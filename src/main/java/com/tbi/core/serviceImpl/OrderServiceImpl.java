/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Order;
import com.tbi.core.models.OrderState;
import com.tbi.core.models.Params;
import com.tbi.core.models.ResponseCode;
import com.tbi.core.models.Store;
import com.tbi.core.repository.OrderCustomerRepository;
import com.tbi.core.repository.OrderRepsitory;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.service.OrderService;
import com.tbi.core.service.ParamService;
import com.tbi.core.service.ProviderService;
import com.tbi.core.utility.Utils;

import okhttp3.Response;

/**
 * @author shameem
 *
 */

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepsitory orderRepository;
    @Autowired
    OrderCustomerRepository orderCustomerRepository;
    @Autowired
    ProviderService providerService;
    @Autowired 
    ApplicationContext applicationContext;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    ParamService paramService;
    
    private static Logger logger = Logger.getLogger(OrderServiceImpl.class);

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order updateOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void delete(Integer orderId) {
        Order order = orderRepository.findOne(orderId);
        orderRepository.delete(order);
    }

    @Override
    public Response updateOrderStatus(Integer orderId, OrderState orderStatus,String storeId) throws Exception {
        List<OrderState> orderStates = new ArrayList<>();
        orderStatus.setDate(Utils.getDate());
        orderStates.add(orderStatus);
        Order order = new Order();
        order = orderRepository.findOne(orderId);
        order.setCurrentState(orderStatus.getStatus());
        order.setMessage(orderStatus.getMessage());
        List<OrderState> mergeList = Stream.concat(order.getOrderStates().stream(), orderStates.stream())
                .collect(Collectors.toList());
        order.setOrderStates(mergeList);
        Order response = orderRepository.save(order);
        if (response != null) {
         return updateOrderStatus(response,storeId);
        }
        return null;
    }

    private Response updateOrderStatus(Order order, String storeId) throws Exception {
        Store store = storeRepository.findStoreByRefId(storeId);
        ProviderService providerService = (ProviderService) applicationContext.getBean(order.getProvider());
        return providerService.updateOrderStatus(order,store.getAuthKey());
    }

    /**
     * 1. get order by id 2. for loop in order status list and get the corresponding object of orderstatus based on surrent stte and revert to controller
     * 
     * @param order
     * @return
     * @return
     * @throws Exception
     */
    @Override
    public OrderState findByCurrentState(Integer orderId) {
        Order order = orderRepository.findOne(orderId);
        Optional<OrderState> stateOptional = order.getOrderStates().stream().filter(state -> state.getStatus().equals(order.getCurrentState())).findAny();
        if (stateOptional.isPresent()) {
            return stateOptional.get();
        }
        return null;
    }

    @Override
    public List<Order> getThirdPartyOrdersByStoreIdAndStatus(Integer storeId, String currentState){
        List<Order> list = orderRepository.findOrdersByStoreIdAndCurrentState(storeId,currentState);
        return list;
    }

    @Override
    public List<Order> findOrdersByStoreIdAndcurrentState(Integer storeId, String currentState) {
        return orderRepository.findOrdersByStoreIdAndCurrentStateOrderByIdDesc(storeId,currentState);
    }

    @Override
    public Order findOrderByRefId(Integer refId) {
        return orderRepository.findOrderByRefId(refId);
    }

	@Override
	public Order findById(Integer id) {
		return orderRepository.findOne(id);
	}

	@Override
	public List<Order> findByStoreIdAndUserId(Integer storeId, Long userId) {
		return orderRepository.findByStoreIdAndUserId(storeId, userId);
		
	}

	@Override
	public Order findTopByOrderByRefIdDesc() {
		return orderRepository.findTopByOrderByRefIdDesc();
	}

	@Override
	public Integer OrderIdGenerator() {
		Order max=findTopByOrderByRefIdDesc();
		int id=0;			
			if(max !=null){
				Integer maxId=max.getRefId();
			maxId = maxId+1;
			id = maxId;
			}else{
				Params paramList = paramService.findByTypeAndClientId(ResponseCode.GENERATE_ORDER_ID, 1).get(0);						
					if (paramList.getKey().equals("GENERATE_ORDER_REF_ID")) {
						 id = paramList.getRefId();						
					}
			}
		return id;
			
	}
	
}
