/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tbi.core.models.Client;
import com.tbi.core.repository.CustomerRechargeRepository;
import com.tbi.core.repository.ClientRepository;
import com.tbi.core.service.ClientService;

/**
 * @author shameem
 *
 */
@Service
public class ClientServiceImpl implements ClientService{
	
	@Autowired
	ClientRepository customerRepository;
	@Autowired
	CustomerRechargeRepository customerRechargeRepository;
    @Autowired
    StoreServiceImpl storeService;

	@Override
	public List<Client> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public Client save(Client customer) throws Exception {
      Client client = customerRepository.save(customer);
      storeService.saveStore(client);
      return client;
	}

	@Override
	public Client findById(Integer customerId) {
		return customerRepository.findOne(customerId);
	}

	@Override
	public void delete(Client customer) {
		customerRepository.delete(customer);
	}

}
