package com.tbi.core.serviceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tbi.core.models.Option;
import com.tbi.core.repository.OptionRepository;
import com.tbi.core.service.OptionService;

@Service
public class OptionServiceImpl implements OptionService {
	@Autowired
	private OptionRepository optionRepository;

	@Override
	public Option saveOption(Option option) {
		return optionRepository.save(option);
	}

	@Override
	public List<Option> fetchAllOption() {
		return optionRepository.findAll();
	}

	@Override
	public void updateOption(Option option) {
		optionRepository.save(option);
	}

	@Override
	public Option findById(Integer id) {
		return optionRepository.findOne(id);
	}
}
