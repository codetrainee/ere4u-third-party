/**
 * 
 */
package com.tbi.core.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.tbi.core.models.DeliveryPerson;
import com.tbi.core.models.Order;
import com.tbi.core.models.RiderDetails;
import com.tbi.core.models.Store;
import com.tbi.core.repository.OrderRepsitory;

import com.tbi.core.repository.RiderRepository;
import com.tbi.core.repository.StoreRepository;
import com.tbi.core.service.ProviderService;
import com.tbi.core.service.RiderService;

/**
 * @author shameem
 *
 */
@Service
public class RiderServiceImpl implements RiderService {

    @Autowired
    private RiderRepository riderRepository;
    
    @Autowired
    private OrderRepsitory orderRepository;
    
    @Autowired
    ApplicationContext applicationContext;
    
    @Autowired
    private StoreRepository storeRepository;
    
    
    private static Logger logger = Logger.getLogger(OrderServiceImpl.class);

    @Override
    public List<DeliveryPerson> findAll() {
        return riderRepository.findAll();
    }

    @Override
    public DeliveryPerson save(DeliveryPerson deliveryPerson) {
        return riderRepository.save(deliveryPerson);
    }

    @Override
    public DeliveryPerson findById(Integer riderId) {
        return riderRepository.findOne(riderId);
    }

    @Override
    public void update(DeliveryPerson deliveryPerson) {
        riderRepository.save(deliveryPerson);
    }

    @Override
    public void delete(Integer itemId) {
       riderRepository.delete(itemId);
    }

    @Override
    public void updateRiderStatus(RiderDetails riderDetails, Integer orderId) {
        Order order = new Order();
        order.setId(orderId);
        order =  orderRepository.findOrderByRefId(riderDetails.getRefId());
        order.setRiderDetails(riderDetails);
        Order response = orderRepository.save(order);
        if(response != null) {
            try {
               updateRiderStatus(response);
            }catch(Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

    private void updateRiderStatus(Order response) {
        Store store = storeRepository.findStoreByRefId(String.valueOf(response.getStoreId()));
        ProviderService providerService = (ProviderService) applicationContext.getBean(response.getProvider());
        providerService.updateRiderStatus(response,store.getAuthKey());
    }

    @Override
    public RiderDetails findRiderByOrderId(Integer orderId) {
        Order order = orderRepository.findOne(orderId);
        if(order.getRiderDetails() != null) {
            return order.getRiderDetails();
        }
       return null ; 
    }
  
}
