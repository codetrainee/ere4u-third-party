package com.tbi.core.serviceImpl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tbi.core.models.User;
import com.tbi.core.repository.UserRepository;
import com.tbi.core.service.UserService;
/* Author Shrikant Mishra
	Date ==>7/07/2020
*/
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	private static Logger logger = Logger.getLogger(UserServiceImpl.class);
	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	@Override
	public void delete(Long userId) {
		User user=userRepository.findOne(userId);
		userRepository.delete(user);
		
	}
	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}
	@Override
	public User saveRegister(User userreg) {
		return userRepository.save(userreg);
	}
	@Override
	public boolean isUserExist(Long mobileNo) {
		return userRepository.exists(mobileNo);
	}
	@Override
	public User findByMobileNo(Long mobileNo) {
		return userRepository.findByMobileNo(mobileNo);
	}
	@Override
	public User uploadImages(User image) {
		return userRepository.save(image);
	}
	@Override
	public Optional<User> findByUserId(Long userId) {
		return userRepository.findByUserId(userId);
	}
	@Override
	public void saveImage(MultipartFile imageFile) throws Exception {
		String folder ="/photos/";
		byte[] bytes=imageFile.getBytes();
		Path path = Paths.get(folder + imageFile.getOriginalFilename());
		Files.write(path, bytes);
	}
	@Override
	public User findByUsername(String userName) {
		return userRepository.findByUsername(userName);
	}
	@Override
	public User findByUserId1(Long userId) {
		// TODO Auto-generated method stub
		return userRepository.findOne(userId);
	}
	
}
